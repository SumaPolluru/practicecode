package filepackage;
import java.io.*;
public class FileDemo {
	FileInputStream inputstream;
	public FileDemo()
	{
		try
		{
		inputstream=new FileInputStream("C:\\HCL\\Hello.txt");
		}
		catch(IOException ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	public void readData()
	{
		try {
			byte b[]=inputstream.readAllBytes();
			for(int i=0;i<b.length;i++)
			{
				System.out.print((char)b[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void writeData()
	{
		String s="Welcome to java application";
		byte arr[]=s.getBytes();
		try
		{
		FileOutputStream fileoutput=new FileOutputStream
				("C:\\HCL\\Hello1.txt");
		fileoutput.write(arr);
		}
		catch(IOException ex )
		{
			System.out.println(ex.getMessage());
		}
	}
	public static void main(String[] args) {
		FileDemo filedemo=new FileDemo();
		filedemo.readData();
		filedemo.writeData();
	}
}
