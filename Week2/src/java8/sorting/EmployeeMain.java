package java8.sorting;
import java.util.*;
public class EmployeeMain {
		public static void main(String[] args) {
			List<Employee> emplist=new ArrayList<Employee>();
			Employee e1=new Employee(1001,"Vanshika",90000.0);
			Employee e2=new Employee(1002,"Harsh",45000.0);
			Employee e3=new Employee(1003,"Anaisha",80000.0);
			Employee e4=new Employee(1004,"Namita",70000.0);
			emplist.add(e1);
			emplist.add(e2);
			emplist.add(e3);
			emplist.add(e4);
			System.out.println("Before java 8 sorting");
			Comparator<Employee> comp=new Comparator<Employee>()
					{
				@Override
					public int compare(Employee emp,Employee emp1)
					{
						return ((emp.getSalary()).
								compareTo(emp1.getSalary()));
					}
				};
			Collections.sort(emplist,comp);
			System.out.println("After sorting data is ");
			for(Employee e:emplist)
			{
				System.out.println(e.getEmpname()+"   "+e.getSalary());
			}
			System.out.println("Using Lambda Expression");
			Comparator<Employee> sortingbysalary=(emp1,emp2)->
			{
				return (emp1.getSalary()).compareTo(emp2.getSalary());
			};
			Collections.sort(emplist,sortingbysalary.reversed());
			System.out.println("After sorting ");
			emplist.forEach(
					e->
					{
						System.out.println("Employee name is "+e.getEmpname()+"  "+e.getSalary());	
					});
			
					}
		}


