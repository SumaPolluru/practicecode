package product;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
public class ProductDAOImpl implements ProductDAO{
	private Set<Product> emplist;
	private Scanner sc;
	public ProductDAOImpl()
	{
		emplist=new HashSet<Product>();
		sc=new Scanner(System.in);
	}
	@Override
	public void addDetails() {

		System.out.println("Enter no of products u want to add");
		int noofproducts=sc.nextInt();
		for(int x=1;x<=noofproducts;x++)
		{
			Product pd=new Product();
			System.out.println("Enter Product id ");
			pd.setProductId(sc.nextInt());
			System.out.println("Enter Product name ");
			pd.setProductName(sc.next());
			System.out.println("Enter Product Quantity ");
			pd.setProductQuantity(sc.nextDouble());
			System.out.println("Enter Product Price");
			pd.setProductPrice(sc.nextDouble());
			emplist.add(pd);
			
		}
		
	}

	@Override
	public void displayDetails() {
		for(Product e:emplist)
		{
			System.out.println("Product id is "+e.getProductId());
			System.out.println("Product name is "+e.getProductName());
			System.out.println("Product Quantity is "+e.getProductQuantity());
			System.out.println("Product price is "+e.getProductPrice());
		}
	}

	@Override
	public void deleteDetails() {
			System.out.println("Enter the product id which u want to delete");
			int empid=sc.nextInt();
			boolean result=false;
			Product pddeleted=null;
			for(Product e1:emplist)
			{
				if(e1.getProductId()==empid)
				{
					result=true;
					pddeleted=e1;
					System.out.println("Object removed");
				}
			}
			if(result)
			{
				emplist.remove(pddeleted);
			}
			else {
				System.out.println("Product id is not present");
			}
	}
}	
