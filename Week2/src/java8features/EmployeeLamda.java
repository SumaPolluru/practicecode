package java8features;
import java.util.*;
public class EmployeeLamda {
private Scanner sc=new Scanner(System.in);
public void operation() {
	EmployeeInterface fname=(firstname,lastname)->{
		return "Your Full name is "+firstname+lastname;
	};
	System.out.println("Enter first name");
	String firstname=sc.next();
	System.out.println("Enter last name");
	String lastname=sc.next();
	String s=fname.fullname(firstname,lastname);
	System.out.println(s);
	}
public static void main(String[] args) {
	EmployeeLamda eobj=new EmployeeLamda();
	eobj.operation();
	
}
}
