package java8features;

public class MessageDemo {
		public void showMessages()
		{
			MessageInterface bdaymessage=(name)->
			{
				return "Happy Birthday!!!"+name;
			};
			String s=bdaymessage.showMessage("Namita");
			System.out.println(s);
			MessageInterface annmessage=(name)->
			{
				return "Hey Its your Anniversary!!!"+name;
			};
			String s1=annmessage.showMessage("Vansh");
			System.out.println(s1);
			MessageInterface congMessage=(name)->
			{
				return "Congratulation "+name+"For your new job";
			};
			String s2=congMessage.showMessage("Harshita");
			System.out.println(s2);
		}
		public static void main(String[] args) {
			MessageDemo messageobj=new MessageDemo();
			messageobj.showMessages();
		}

	}

