package java8features;
@FunctionalInterface
public interface EmployeeInterface {
   public String fullname(String firstname, String lastname);
}
