package java8features;

public class BankDemo {
      public void operation() {
    	  System.out.println("Calculating Interest for Saving account(Interest is 6%)");
    	  BankInterface b=(amount)->{
				System.out.println((amount*6)/100);	// amount*0.06			
			};
			b.calculateInterest(100000);
			System.out.println("Calculating Interest for Current account(Interest is 5%)");
	    	  BankInterface b1=(amount)->{
					System.out.println((amount*5)/100);				
			};
			b1.calculateInterest(856788);
      }
      public static void main(String[] args) {
			BankDemo bobj=new BankDemo();
			bobj.operation();
}
}
