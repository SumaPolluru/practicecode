package java8features;
public class LambdaDemo {
		public void operations()
		{
			System.out.println("Finding Addition ");
			Calculate c1=(num1,num2)->{
				System.out.println("Sum is "+(num1+num2));
				
			};
			c1.calculate(90,100);
			System.out.println("Finding subtraction");
			Calculate c2=(num1,num2)->
			{
				System.out.println("Difference is "+(num1-num2));
			};
			c2.calculate(90, 40);
		}
		public static void main(String[] args) {
			LambdaDemo lambobj=new LambdaDemo();
			lambobj.operations();
		}

}
