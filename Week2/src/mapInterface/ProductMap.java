package mapInterface;

import java.util.Objects;

public class ProductMap {
	private int productId;
	private String productName;
	private int quantity;

	public ProductMap(int productId, String productName, int quantity) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		return Objects.hash(productId, productName, quantity);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductMap other = (ProductMap) obj;
		return productId == other.productId && Objects.equals(productName, other.productName)
				&& quantity == other.quantity;
	}

	@Override
	public String toString() {
		return "[productId=" + productId + ", productName=" + productName + ", quantity=" + quantity + "]";
	}

	public String toString1() {
		return "[productId=" + productId + ", productName=" + productName + "]";
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
