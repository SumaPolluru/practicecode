package mapInterface;
import java.util.*;
public class MapDemo {
	private Map<Integer,String> mapd;
	public MapDemo()
	{
		mapd=new HashMap<Integer,String>();
	}
	public void insert()
	{
		mapd.put(1001, "FirstVal");
		mapd.put(1002, "SecondVal");
		mapd.put(1003, "ThirdVal");
		mapd.put(1004, "ForthVal");
		mapd.put(1005, "SixthVal");
	}
	public void display()
	{
		Set<Map.Entry<Integer,String>> mapentry=mapd.entrySet();
		for(Map.Entry<Integer, String> oneentry:mapentry)
		{
			System.out.println("Key is "+oneentry.getKey());
			System.out.println("Value is "+oneentry.getValue());
		}
	}
	public static void main(String[] args) {
		MapDemo mapd=new MapDemo();
		mapd.insert();
		mapd.display();
	}

}
