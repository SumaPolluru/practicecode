package Dao;
public interface EmployeeIntr {
	public void addDetails();
	public void updateDetails();
	public void deleteDetails();
	public void displayDetails();
}
