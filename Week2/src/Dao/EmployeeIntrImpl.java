package Dao;
import pojo.Employee;
import java.util.*;
public class EmployeeIntrImpl implements EmployeeIntr {
	private Set<Employee> emplist;
	private Scanner sc;
	public EmployeeIntrImpl()
	{
		emplist=new HashSet<Employee>();
		sc=new Scanner(System.in);
	}
	@Override
	public void addDetails() {
		System.out.println("Enter no of employees u want to add");
		int noofemployee=sc.nextInt();
		for(int x=1;x<=noofemployee;x++)
		{
			Employee emp=new Employee();
			System.out.println("Enter employee id ");
			emp.setEmpId(sc.nextInt());
			System.out.println("Enter Employee name ");
			emp.setEmpName(sc.next());
			System.out.println("Enter Salary ");
			emp.setSalary(sc.nextDouble());
			emplist.add(emp);
			
		}
		
	}

	@Override
	public void displayDetails() {
		for(Employee e:emplist)
		{
			System.out.println("Employee code is "+e.getEmpId());
			System.out.println("Employee name is "+e.getEmpName());
			System.out.println("Employee salary is "+e.getSalary());
		}
	}

	@Override
	public void deleteDetails() {
			System.out.println("Enter the employee id which u want to delete");
			int empid=sc.nextInt();
			boolean result=false;
			Employee empdeleted=null;
			for(Employee e1:emplist)
			{
				if(e1.getEmpId()==empid)
				{
					result=true;
					empdeleted=e1;
					System.out.println("Object removed");
				}
			}
			if(result)
			{
				emplist.remove(empdeleted);
			}
			else {
				System.out.println("Employee id is not present");
			}
		}

	@Override
	public void updateDetails() {
		System.out.println("Enter the employee id");
		int empid=sc.nextInt();
		for(Employee e1:emplist) {
			if(e1.getEmpId()==empid) {
		     System.out.println("Enter new  salary");
		    e1.setSalary(sc.nextDouble());
		     System.out.println("Salary is updated");
			}
		}
		}
}
