package com.gl.main.dao;

import com.gl.main.pojo.Visitor;

public interface VisitorDAO {
	public void seeTask(Visitor vobj);
	public void markTask(Visitor vobj);
}
