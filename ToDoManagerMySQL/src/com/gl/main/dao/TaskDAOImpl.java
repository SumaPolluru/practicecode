package com.gl.main.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gl.main.connect.*;
import com.gl.main.pojo.Client;
import com.gl.main.pojo.Task;

public class TaskDAOImpl implements TaskDAO {
	private Connection con1;
	private PreparedStatement stat;// PreparedStatement is and interface in java.sql
	// which can be used to write queries using java to database.

	public TaskDAOImpl() {
		con1 = DataConnect.getConnect();
	}

	@Override
	public void insertTask(Task tobj) {
		try {
			stat = con1.prepareStatement("Insert into Task values(?,?,?,?,?,?)");
			stat.setInt(1, tobj.getTaskId());
			stat.setString(2, tobj.getTaskTitle());
			stat.setString(3, tobj.getTaskText());
			stat.setString(4, tobj.getTaskassignedTo());
			stat.setString(5, tobj.getCompletionDate());
			stat.setString(6, tobj.getCompletionTime());
			//stat.setString(5, String.valueOf(tobj.getCompletionDate()));
			//stat.setString(6, String.valueOf(tobj.getCompletionTime()));
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data inserted");
			}
		} catch (SQLException e) {
			System.out.println("Exception occurs!!");
		}

	}
	@Override
	public void updateTask(Task tupdate) {
		try {
			stat = con1.prepareStatement("update task set taskassignedTo =? where TaskId=?");
			stat.setString(1, tupdate.getTaskassignedTo());
			stat.setInt(2, tupdate.getTaskId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data updated success");
			}
		} catch (Exception ex) {
			System.out.println("TaskID is present");
		}
	}

	@Override
	public void deleteTask(Task tdelete) {
		try {
			stat = con1.prepareStatement("delete from task where taskId=?");
			stat.setInt(1, tdelete.getTaskId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data deletion success");
			}
		} catch (Exception ex) {
			System.out.println("Task ID is not present");
		}

	}

	@Override
	public List<Task> retreiveData() {
		List<Task> emplist = new ArrayList<Task>();
		try {
			stat = con1.prepareStatement("select * from task");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Task e1 = new Task();
				e1.setTaskId(result.getInt(1));
				e1.setTaskTitle(result.getString(2));
				e1.setTaskText(result.getString(3));
				e1.setTaskassignedTo(result.getString(4));
				e1.setCompletionDate(result.getString(5));
				e1.setCompletionTime(result.getString(6));
				emplist.add(e1);

			}
		} catch (SQLException e) {
			System.out.println("Displaying operation unsuccessful");
		}

		return emplist;
	}
	@Override
	public void searchTask(Task tobj) {
		try {
			stat = con1.prepareStatement("select TaskTitle from task where taskId=?");
			stat.setInt(1, tobj.getTaskId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data Found in the Database:");
			}
		} catch (Exception ex) {
			System.out.println("Task Id not present in the database");
		}

		
	}

	@Override
	public void completionDate(Task tobj) {
		try {
			stat = con1.prepareStatement("update task set completionDate =? where TaskId=?");
			stat.setString(1, tobj.getCompletionDate());
			stat.setInt(2, tobj.getTaskId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Successfully added the completion date for given Task Id");
			}
		} catch (Exception ex) {
			System.out.println("TaskID is not present");
		}
		
	}

	@Override
	public void assignTo(Task tobj) {
			try {
				stat = con1.prepareStatement("update task set taskassignedTo =? where TaskId=?");
				stat.setString(1, tobj.getTaskassignedTo());
				stat.setInt(2, tobj.getTaskId());
				int result = stat.executeUpdate();
				if (result > 0) {
					System.out.println("Data assigned success");
				}
			} catch (Exception ex) {
				System.out.println("Task Assignation Unsuccessful");
			}
		
	}

	@Override
	public List<Task> sortData() {
		List<Task> emplist = new ArrayList<Task>();
		try {
			stat = con1.prepareStatement("select * from task order by taskId asc");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Task e1 = new Task();
				e1.setTaskId(result.getInt(1));
				e1.setTaskTitle(result.getString(2));
				e1.setTaskText(result.getString(3));
				e1.setTaskassignedTo(result.getString(4));
				e1.setCompletionDate(result.getString(5));
				e1.setCompletionTime(result.getString(6));
				emplist.add(e1);

			}
		} catch (SQLException e) {
			System.out.println("Sorting operation unsuccessful");
		}

		return emplist;
	}
	}	