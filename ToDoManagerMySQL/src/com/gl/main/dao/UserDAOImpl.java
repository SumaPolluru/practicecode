package com.gl.main.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gl.main.connect.*;
import com.gl.main.pojo.Task;
import com.gl.main.pojo.User;

public class UserDAOImpl implements UserDAO {
	private Connection con1;
	private PreparedStatement stat;// PreparedStatement is and interface in java.sql
	// which can be used to write queries using java to database.

	public UserDAOImpl() {
		con1 = DataConnect.getConnect();
	}

	@Override
	public void insertUser(User tobj) {
		try {
			stat = con1.prepareStatement("Insert into User values(?,?,?)");
			stat.setInt(1, tobj.getUserId());
			stat.setString(2, tobj.getUserName());
			stat.setString(3, tobj.getPassword());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data inserted");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@Override
	public void updateUser(User tupdate) {
		try {
			stat = con1.prepareStatement("update User set Password =? where UserId=?");
			stat.setString(1, tupdate.getPassword());
			stat.setInt(2, tupdate.getUserId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data updated success");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void deleteUser(User tdelete) {
		try {
			stat = con1.prepareStatement("delete from user where userid=?");
			stat.setInt(1, tdelete.getUserId());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data deletion success");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	@Override
	public List<User> displayUser() {
		List<User> emplist = new ArrayList<User>();
		try {
			stat = con1.prepareStatement("select * from user");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				User e1 = new User();
				e1.setUserId(result.getInt(1));
				e1.setUserName(result.getString(2));
				e1.setPassword(result.getString(3));
				emplist.add(e1);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emplist;
	}

	@Override
	public boolean checkUser(String username,String password)
	{
		if(username.equals("Suma")&&password.equals("Chintu"))
		{
			return true;
		}
		else
		{
			return false;
       }
}

}