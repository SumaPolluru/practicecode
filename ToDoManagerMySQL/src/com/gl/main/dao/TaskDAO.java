package com.gl.main.dao;
import java.util.List;
import com.gl.main.pojo.Client;
import com.gl.main.pojo.Task;
public interface TaskDAO {
	public void insertTask(Task tobj) ;
	public void deleteTask(Task tobj);
	public void updateTask(Task tobj);
	public List<Task> retreiveData();
	public void searchTask(Task tobj);
	public void completionDate(Task tobj);
	public void assignTo(Task tobj);
	public List<Task> sortData();
}
