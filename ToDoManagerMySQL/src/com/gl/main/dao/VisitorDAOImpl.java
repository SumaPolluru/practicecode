package com.gl.main.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.gl.main.connect.DataConnect;
import com.gl.main.pojo.Visitor;

public class VisitorDAOImpl implements VisitorDAO{
	private Connection con1;
	private PreparedStatement stat;// PreparedStatement is and interface in java.sql
	// which can be used to write queries using java to database.

	public VisitorDAOImpl() {
		con1 = DataConnect.getConnect();
	}
	@Override
	public void seeTask(Visitor vobj) {
	try {
		stat = con1.prepareStatement("select t.taskId,t.taskTitle,t.taskText,t.completionDate,v.visitorname from task t join visitor v where t.taskId=?");
		stat.setInt(1, vobj.getTaskId());
		int result = stat.executeUpdate();
		if (result > 0) {
			System.out.println("Assigned Tasks to the visitor");
		}
	} catch (Exception ex) {
		System.out.println("Task ID is not present");
	}
	}

	@Override
	public void markTask(Visitor vobj) {
		try {
			stat = con1.prepareStatement("update visitor set markstatus =? where taskId=? and visitorName=?");
			stat.setString(1, vobj.getMarkstatus());
			stat.setInt(2, vobj.getTaskId());
			stat.setString(3, vobj.getVisitorName());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data Marked successfully");
			}
		} catch (Exception ex) {
			System.out.println("No Tasks assigned");
		}
		
	}

}
