package com.gl.main.dao;

import java.util.List;

import com.gl.main.pojo.Task;
import com.gl.main.pojo.User;

public interface UserDAO {
	public  void insertUser(User tobj);
	public  void deleteUser(User tobj);
	public  void updateUser(User tobj);
	public List<User> displayUser();
	public  boolean checkUser(String username,String password);
}
