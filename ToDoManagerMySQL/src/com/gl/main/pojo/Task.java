package com.gl.main.pojo;

import java.time.LocalDate;
import java.time.LocalTime;

public class Task {
	private int taskId;
	private String taskTitle;
	private String taskText;
	private String taskassignedTo;
	public String completionDate;
	public String getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	public String getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(String completionTime) {
		this.completionTime = completionTime;
	}

	public String completionTime;
	

	/*public LocalDate getCompletionDate() {
		this.completionDate = LocalDate.now();
		return completionDate;
	}

	public LocalTime getCompletionTime() {
		this.completionTime = LocalTime.now();
		return completionTime;
	}

	public LocalTime completionTime;*/

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public String getTaskText() {
		return taskText;
	}

	public void setTaskText(String taskText) {
		this.taskText = taskText;
	}

	public String getTaskassignedTo() {
		return taskassignedTo;
	}

	public void setTaskassignedTo(String taskassignedTo) {
		this.taskassignedTo = taskassignedTo;
	}

}
