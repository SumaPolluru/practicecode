package employeeeeinfo.pjo;

public class Department {
	private int deptCode;
	private String deptname;
	private Employee[] emparr;

	public int getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(int deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptname() {
		return deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public Employee[] getEmparr() {
		return emparr;
	}

	public void setEmparr(Employee[] emparr) {
		this.emparr = emparr;
	}

}
