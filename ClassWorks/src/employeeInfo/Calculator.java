package employeeInfo;

public class Calculator {
	public void add()
	{
		System.out.println("Add method of Calculator");
	}
	public void subtract()
	{
		System.out.println("Subtract method of Calculator");
	}
	public void multiply()
	{
		System.out.println("Multiply method of Calculator");
	}
	public static void main(String[] args) {
		Calculator cobj;
		cobj=new Calculator();
		cobj.add();
		cobj.subtract();
		cobj.multiply();
	}

}
