package claspractice.exceptiondemo;

import java.util.Scanner;

public class AmountException {
	private Scanner sc;
public AmountException() {
	sc=new Scanner(System.in);
}
	public void amountCheck() {
		int balance=5000;
		System.out.println("balance in bank is :"+balance);
		System.out.println("Enter the amount");
		int amount=sc.nextInt();
		try {
		if(amount>balance) {
		 throw new InvalidAmountException();
		}
		}
		catch(InvalidAmountException e) {
			System.out.println(e.getMessage());
		}
	}
    public static void main(String args[]) {
    AmountException amobj=new AmountException();
    amobj.amountCheck();
    }
}
