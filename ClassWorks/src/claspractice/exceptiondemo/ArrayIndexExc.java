package claspractice.exceptiondemo;
import java.util.*;
public class ArrayIndexExc {
private Scanner sc;
private int arr[];
public ArrayIndexExc() {
	sc=new Scanner(System.in);
}
public void arrayIndex() {
	try {
	System.out.println("Enter the size of array");
	int size = sc.nextInt();
	arr=new int[size];
	System.out.println("Enter the tasks to be stored in the array:");
	for(int x=0;x<=size;x++)
	{
	arr[x]=sc.nextInt();
	}
	}
	catch(ArrayIndexOutOfBoundsException e) {
		System.out.println("Array exceeds the size of array");	
	}
}
public static void main(String[] args) {
	ArrayIndexExc eobj=new ArrayIndexExc();
	eobj.arrayIndex();
}
}
