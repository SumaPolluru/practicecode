package claspractice.exceptiondemo;
import java.util.*;
public class MultiException {
	private Scanner sc;
	int arr[];
	public MultiException()
	{
		sc=new Scanner(System.in);
		arr=new int[4];
	}
	public void accept()
	{
		for(int x=0;x<arr.length;x++)
		{
			System.out.println("Enter any number ");
			arr[x]=sc.nextInt();
		}
	}
	public void divide()
	{
		System.out.println("Enter index number1 for first values");
		int index1=sc.nextInt();
		System.out.println("Enter index number 2 for second value");
		int index2=sc.nextInt();
		try
		{
		double result=arr[index1]/arr[index2];
		System.out.println("Result is   "+result);
		}
		catch(ArithmeticException ex)
		{
			System.out.println("trying to divide by zero");
		}
		catch(ArrayIndexOutOfBoundsException arr)
		{
			System.out.println("Trying to access beyound limit");
		}
		catch(Exception ex)
		{
			System.out.println("exception is "+ex.getMessage());
		}
	}
	public static void main(String[] args) {
		MultiException multi=new MultiException();
		multi.accept();
		multi.divide();
	}

}

