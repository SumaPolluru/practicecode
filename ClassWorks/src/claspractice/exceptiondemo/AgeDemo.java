package claspractice.exceptiondemo;
import java.util.*;
public class AgeDemo {
	int age;
	private Scanner sc;
	public AgeDemo()
	{
		sc=new Scanner(System.in);
	}
	/*
	 * throw can be used to tell jVM explicitly that this exception should come
	 */
	public void checkAge()
	{
		System.out.println("Enter Age");
		age=sc.nextInt();
		try
		{
		if(age<18)
		{
			throw new InvalidAgeException();
		}
		}
		catch(InvalidAgeException ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	public static void main(String[] args) {
		AgeDemo ageobj=new AgeDemo();
		ageobj.checkAge();
	}
}
