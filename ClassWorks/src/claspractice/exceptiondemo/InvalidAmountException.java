package claspractice.exceptiondemo;

public class InvalidAmountException extends Exception
{
	@Override
	public String getMessage()
	{
		return "Amount is exceed than balance in account";
	}
}
