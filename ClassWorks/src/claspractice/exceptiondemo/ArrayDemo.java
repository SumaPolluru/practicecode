package claspractice.exceptiondemo;
import java.util.*;
public class ArrayDemo {
	private int var[];
	private Scanner sc;
	public ArrayDemo()
	{
		var=new int[5];
		sc=new Scanner(System.in);
	}
	public void accept()
	{
		for(int x=0;x<var.length;x++)
		{
			System.out.println("Enter Value ");
			var[x]=sc.nextInt();
					
		}
	}
	public void display()
	{
		try
		{
		
		for(int counter=0;counter<=var.length;counter++)
		{
			System.out.println("value is "+var[counter]);
		}
		
		}
		catch(ArrayIndexOutOfBoundsException arr)
		{
			System.out.println("Trying to access beyond limit");
		}
	}
	public static void main(String[] args) {
		ArrayDemo arr=new ArrayDemo();
		arr.accept();
		arr.display();
	}

}
