package claspractice.exceptiondemo;
import java.util.*;
public class ArithDemo {
	private Scanner sc;
	public ArithDemo()
	{
		sc=new Scanner(System.in);

	}
	public void calculate()
	{
		System.out.println("Enter Number 1");
		int num1=sc.nextInt();
		System.out.println("Enter Number 2");
		int num2=sc.nextInt();
		try
		{
		double result=num1/num2;
		System.out.println("Result is "+result);
		}
		catch(ArithmeticException ex)
		{
			System.out.println("Trying to divide by zero");
		}
	}
	public static void main(String[] args) {
		ArithDemo ariths=new ArithDemo();
		ariths.calculate();
	}

}
