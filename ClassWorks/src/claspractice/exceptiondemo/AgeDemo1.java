package claspractice.exceptiondemo;
import java.util.*;
public class AgeDemo1 {
	int age;
	private Scanner sc;
	public AgeDemo1()
	{
		sc=new Scanner(System.in);
	}
	/*
	 * throw can be used to tell jvm explicitly that this exception should come
	 */
	public void checkAge()throws InvalidAgeException
	{
		System.out.println("Enter Age");
		age=sc.nextInt();
		
		throw new InvalidAgeException();
		
	}
	public static void main(String[] args) {
		AgeDemo1 ageobj=new AgeDemo1();
		try
		{
		ageobj.checkAge();
		}
		catch(InvalidAgeException ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	

}
/*
 * throws define the list of checked exception which I don't want to handle in current method.
 * it is delegating the task of exception handling to calling method.
 * *
 */
