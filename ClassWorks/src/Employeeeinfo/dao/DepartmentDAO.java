
package Employeeeinfo.dao;
public interface DepartmentDAO {
	public void insertDepartment();
	public void deleteDepartment();
	public void updateDepartment();
	public void retreiveDepartment();
}
