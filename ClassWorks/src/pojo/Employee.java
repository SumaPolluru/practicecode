package pojo;
public class Employee {
	private int empid;//class variable
	private String empname;
	private double salary;
	public void setEmpid(int empid)
	{
		this.empid=empid;
		//parameter empid will be assign to class variable empid;
	}
	public int getEmpid()
	{
		return this.empid;
	}
	public void setEmpname(String empname)
	{
		this.empname=empname;
		
	}
	public String getEmpname()
	{
		return this.empname;
	}
	public void setSalary(Double salary) {
		this.salary=salary;
	}
    public Double getSalary(){
    	return this.salary;
    }
} 
