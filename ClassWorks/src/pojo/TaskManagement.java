package pojo;
import java.util.*;
public class TaskManagement {
	private Scanner sc;
	Task taskobj[];
	public TaskManagement()
	{
		sc=new Scanner(System.in);
		taskobj=new Task[5];
	}
	public void menuMethod() {
	int option=1;
	while(option>0)
	{ 
	    System.out.println("Displaying the Menu");
		System.out.println("1. Create");
		System.out.println("2. Delete");
		System.out.println("3. Update");
		System.out.println("4. Search");
		System.out.println("5. Display");
		System.out.println("6. Exit");
	    System.out.println("Please enter the task operation number");
	int menu=sc.nextInt();
	switch(menu)
	{
	case 1: createDetails();
	          break;
	case 2: deleteDetails();
	        break;
	case 3: updateDetails();
	         break;
	case 4: searchDetails();
	         break;
	case 5: displayDetails();
	         break;
	         
	case 6: System.out.println("Successfully logout, Thank You!");
		    System.exit(0);
	        break;
	default:
		System.out.println("Invalid option");
	}
	System.out.println("Would You like to continue[please enter number]");
    System.out.println("For Continue press any positive number except zero");
    System.out.println("For Exit press 0");
    option=sc.nextInt();
	}
	}
	public void createDetails()
	{
		for(int index=0;index<taskobj.length;index++)
		{
			taskobj[index]=new Task();
			System.out.println("Enter the task Id ");
			taskobj[index].setTaskId(sc.nextInt());
			System.out.println("Enter the Task Title");
			taskobj[index].setTaskTitle(sc.next());
			System.out.println("Enter the Task Text");
			taskobj[index].setTaskText(sc.next());
			System.out.println("Enter the name to whom,the task should assign");
			taskobj[index].setAssignTo(sc.next());
			
		}
	}
    public void deleteDetails() {
    	System.out.println("Enter TaskId for which u want to make deletion");
    	int taskId=sc.nextInt();
    	for(Task eobj:taskobj) {
    		if(eobj.getTaskId()==taskId)
    		{
    		System.out.println("Task Text for this id is deleted");
    		eobj.setAssignTo(null);
    		}
    	}
    	}
    public void displayDetails()
	{
		for(Task tmobj:taskobj)
		{
			System.out.println("Task id is "+tmobj.getTaskId());
			System.out.println("Task Title is "+tmobj.getTaskTitle());
			System.out.println("Task Text is "+tmobj.getTaskText());
			System.out.println("Task Text is deleted "+tmobj.getTaskText());
			System.out.println("Task assignation to "+tmobj.getAssignTo());
		}
	}
    public void updateDetails()
	{
			System.out.println("Enter Task Id for which u want to make updation");
			int taskId=sc.nextInt();
			for(Task eobj:taskobj)
			{
				if(eobj.getTaskId()==taskId)
				{
					System.out.println("Enter New Task Title");
					eobj.setTaskTitle(sc.nextLine());
				}
				System.out.println(taskId+"is updated");
			}
	}
	
    public void searchDetails() {
    	System.out.println("Enter task id to search the data");
    	int tid=sc.nextInt();
    	int temp=0;
    	for(Task obj1:taskobj) {
    		if(obj1.getTaskId()==tid) 
    			{
    	    		temp=1;   
    	    	   }
    		    }
    	    	if(temp == 1)
    	         {
    			      System.out.println(" taskid is Found");
    	         }
    		    else
    	         {
    			      System.out.println("taskid Not Found");
    		     }
    	   }
	public static void main(String[] args) {
		TaskManagement taskmain=new TaskManagement();
		taskmain.menuMethod();
	}
	}
