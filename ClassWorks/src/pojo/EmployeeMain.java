package pojo;
import java.util.*;
public class EmployeeMain {
	private Scanner sc;
	Employee emparr[];
	/*Constructor is a special method which has same name as class name
	 * constructor will not return any value 
	 * constructor will call automatically as soon as you create object of class*/
	public EmployeeMain()
	{
		sc=new Scanner(System.in);
		emparr=new Employee[5];
		
	}
	public void accept()
	{
		for(int index=0;index<emparr.length;index++)
		{
			emparr[index]=new Employee();
			System.out.println("Enter Employee code ");
			emparr[index].setEmpid(sc.nextInt());
			System.out.println("Enter Employee name ");
			emparr[index].setEmpname(sc.next());
			System.out.println("Enter Employee Salary");
			emparr[index].setSalary(sc.nextDouble());
			
		}
	}
	public void updateDetails()
	{
			System.out.println("Enter Employee Code for which u want to make updation");
			int empid=sc.nextInt();
			for(Employee eobj:emparr)
			{
				if(eobj.getEmpid()==empid)
				{
					System.out.println("Enter New salary");
					eobj.setSalary(sc.nextDouble());
					
				}
			}
		
	}
    public void deleteDetails() {
    	System.out.println("Enter empoyee code for which u want to make deletion");
    	int empid=sc.nextInt();
    	for(Employee eobj:emparr) {
    		if(eobj.getEmpid()==empid)
    		{
    		System.out.println("Salary of this Id is deleted");
    		eobj.setSalary(null);
    		}
    	}
    	}
	public void display()
	{
		for(Employee emobj:emparr)
		{
			System.out.println("Employee id is "+emobj.getEmpid());
			System.out.println("Employee name is "+emobj.getEmpname());
			System.out.println("Salary is"+emobj.getSalary());
			System.out.println("Salary is deleted");
		}
		
	}
	public static void main(String[] args) {
		EmployeeMain empmain=new EmployeeMain();
		empmain.accept();
		empmain.display();
		System.out.println("After updtion ");
		empmain.updateDetails();
		empmain.display();
		empmain.deleteDetails();
		empmain.display();
	}
	}
