package collection;
import java.util.*;
public class CollectionDemo {
	private Collection<String> namescoll;
	private Scanner sc;
	public CollectionDemo()
	{
		namescoll=new ArrayList<String>();
		sc=new Scanner(System.in);
	}
	public void insertData()
	{
		namescoll.add("John");
		namescoll.add("Sumantha");
		namescoll.add("Vinay");
		namescoll.add("Vanita");
		namescoll.add("Juhi");
		
		
	}
	public void display()
	{
		for(String s:namescoll)
		{
			System.out.println("Name is "+s);
		}
		namescoll.remove("Vinay");
		System.out.println("Names after removing");
		for(String name:namescoll)
		{
			System.out.println("Names are "+name);
		}
	}
	public void search() {
		   System.out.println("Enter name");
		    String name=sc.next();
			if(namescoll.contains(name)) 
			{
		   System.out.println("Found");
			}
		else {
			System.out.println("Not Found");
			}
	}
	public static void main(String[] args) {
		CollectionDemo clobj=new CollectionDemo();
		clobj.insertData();
		clobj.display();
		clobj.search();
	}

}
