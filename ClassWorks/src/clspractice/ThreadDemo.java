package clspractice;
//To create application as multi threaded application just extend from thread class
public class ThreadDemo extends Thread {
	public void run()
	{
		for(int x=1;x<=10;x++)
		{
			try
			{
			System.out.println("Value of x is "+x);
			Thread.sleep(2000);//millisecond
			}
			catch(InterruptedException ex)
			{
				System.out.println("Thread interrupted");
			}
		}
	}
	public static void main(String[] args) {
		ThreadDemo th1=new ThreadDemo();//new Thread stage
		th1.start();//Runnable stage,internally start method
		//will search for run method where u write all code which
		//u want to implement whenever thread started
		try 
		{
			th1.join();
		}
		catch(InterruptedException ex)
		{
			System.out.println("Thread interrupted");
		}
		SecThreadDemo st=new SecThreadDemo();

		try {
			st.join();
		}
		catch(InterruptedException ex)
		{
			System.out.println("Thread interrupted");
		}
		st.start();
	}

}
/*
 * By Default every java application is single threaded application
 * a
 * */
