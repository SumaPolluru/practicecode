package clspractice;
public class StaticDemo {
	static
	{
		System.out.println("Should be first statement even before Constructor");
		System.out.println("Static block can be used to initalize database connection or file");
	}
	public StaticDemo()
	{
		System.out.println("This is Constructor Statement");
	}
	public static void main(String[] args) {
		StaticDemo s1=new StaticDemo();
	}

}
