package clspractice;

public class StaticVar {
	//static means its class level
	 //static int x=30;
	  int x=30;//instance variable
	 public void show()
	{
		System.out.println("Value of x is "+x);
	}
	public static void main(String[] args) {
		StaticVar stdemo=new StaticVar();
		stdemo.x=80;
		stdemo.show();
		StaticVar stdemo1=new StaticVar();
		stdemo1.show();
				
	}

}
/*
 * In instance variable if u make changes in one object, it is not going to affect
 * other objects
 */
