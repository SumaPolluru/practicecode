package clspractice;

public abstract class ShapeClass {
	public ShapeClass()
	{
		System.out.println("Constructor of super class");
	}	
	public void acceptDetails()
	{
		System.out.println("Accepting details of Shape");
	}
	public void displayDetails()
	{
		System.out.println("Displaying details of Shape");
	}
	public abstract void calculateArea();
}
/*Any method which is only declared but implementation will be provided in child classes
 * is called abstract methods.
 * 2)Any class which is containing one or more abstract methods must
 *  be declared as abstract.
 * 3)An abstract class can containing any type of variable.
 * 4)An abstract class can have abstract as well as non abstract methods.
 * */
