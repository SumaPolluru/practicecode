package clspractice;

public class SquareClass extends ShapeClass {

	@Override
	public void calculateArea() {
System.out.println("Calculating are of square");		
	}
	public static void main(String[] args) {
		ShapeClass shapeobj;//you cannot create object of abstract class;
		shapeobj=new RectangleClass();
		shapeobj.acceptDetails();
		shapeobj.displayDetails();
		shapeobj.calculateArea();
		System.out.println("Calling for Square!!!!");
		shapeobj=new SquareClass();
		shapeobj.acceptDetails();
		shapeobj.displayDetails();
		shapeobj.calculateArea();
	}
}
