package clspractice;

public class Rectangle extends Shape {

	public Rectangle()
	{

		System.out.println("Constructor of sub class");
	}
	public static void main(String[] args) {
		Rectangle robj=new Rectangle();
	}
}
//Whenever u create object of sub class, first it will call constructor of super class
//then constructor of sub class.
