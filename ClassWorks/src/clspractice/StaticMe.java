package clspractice;
public class StaticMe {
	//static means its class level
   //static int x=30;
	static int x=30;//instance variable
	static int y=40;
	static void show()
	{
		System.out.println("Value of x is "+x);
	}
	static void add() {
		System.out.println("Value of y is:"+y);
	}
	public static void main(String[] args) {
		show();
		add();
				
	}

}
