package clspractice;

public class SingletonDesignPatternExample {
	private static SingletonDesignPatternExample singleobj;
	private SingletonDesignPatternExample()
	{
		
	}//no one will be able to access object outside class.
	public void display()
	{
		System.out.println("Business method of class");
	}
	public static SingletonDesignPatternExample getObj()
	{
		singleobj=new SingletonDesignPatternExample();
		return singleobj;
	}

}
