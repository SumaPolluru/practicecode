package clspractice;

public class RectangleClass extends ShapeClass {

	public RectangleClass()
	{

		System.out.println("Constructor of sub class");
	}
	@Override
	public void calculateArea() {
    System.out.println("Calculating area of Rectangle");		
	}
}
//Whenever u create object of sub class, first it will call constructor of super class
//then constructor of sub class.
