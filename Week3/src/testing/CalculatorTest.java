package testing;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
public class CalculatorTest {
	
	Calculator cobj;
	@Before//Before testing anythings this method will run
	public void setUp()
	{
		cobj=new Calculator();
	}
	@Test// will tell JVM that this method is used for testing purpose
	public void testAdd()
	{
		assertEquals(cobj.add(20,30),50);
		assertEquals(cobj.add(-9,-8),-17);
		assertEquals(cobj.add(0,7),7);
	}
	@Test
	public void testsub()
	{
		assertEquals(cobj.subtraction(20,30),-10);
		assertEquals(cobj.subtraction(-9,8),-17);
		assertEquals(cobj.subtraction(0,7),-7);
	}
}
