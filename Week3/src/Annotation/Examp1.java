package Annotation;

public class Examp1 {
		public void acceptDetails()
		{
			System.out.println("Accepting Details of Student");
		}
		public void displayDetails()
		{
			System.out.println("Displaying Details of Student");
		}
		public boolean checkUser(String uname,String pwd)
		{
			if(uname.equals("admin")&&pwd.equals("admin"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}
	/*
	 * Changing the functionality or behavior of method which 
	 * is defined in super class.
	 * In Override everything should be same which includes 
	 * method name,method parameter as well as return type
	 * */

