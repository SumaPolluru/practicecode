package Annotation;
import java.util.*;
public class Examp1Extends  extends Examp1{
		@Override
		public void acceptDetails()
		{
			System.out.println("Accept details in tech student");
		}
		@SuppressWarnings("unchecked")
		public void show()
		{
			List mlist=new ArrayList();
			mlist.add("Data");
			mlist.add(90);
			mlist.add(89.0);
			System.out.println(mlist);
					
		}
		@Deprecated
		public void showMessage()
		{
		System.out.println("Welcome to show message");	
		}
		public static void main(String[] args) {
			Examp1Extends eobj=new Examp1Extends();
			eobj.showMessage();
			eobj.acceptDetails();
			eobj.show();
		}


	}
	/*
	 * @Override annotation make sure that method should be overriden
	 * if u write anything wrong it will start giving error.
	 * */

