package day4.factorymethod;

public abstract class Employee {
	public void acceptDetails()
	{
		System.out.println("Accepting Details");
	}
	public void displayDetails()
	{
		System.out.println("Displaying Details ");
	}
	public abstract void calculateSalary();

}
