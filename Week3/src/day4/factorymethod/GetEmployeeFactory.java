package day4.factorymethod;

public class GetEmployeeFactory {
	public Employee getEmployee(String emptype)
	{
		if(emptype.equals("Permanent"))
		{
			return new PermEmployee();
		}
		else if(emptype.equals("Temp"))
		{
			return new TempEmployee();
		}
		return null;

	}
}
