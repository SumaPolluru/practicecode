package day4.factorymethod;
import java.util.*;
public class Main {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Employee type Permanent/Temp");
		String name=sc.next();
		GetEmployeeFactory empfact=new GetEmployeeFactory();
		Employee eobj=empfact.getEmployee(name);
		eobj.acceptDetails();
		eobj.displayDetails();
		eobj.calculateSalary();
	}

}
