package day4.factorymethod;

public class PermEmployee extends Employee{

	@Override
	public void calculateSalary() {
			System.out.println("Calculating salary for Permanent Employee");
	}

}
