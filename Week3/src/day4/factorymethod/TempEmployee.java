package day4.factorymethod;

	public class TempEmployee extends Employee{

		@Override
		public void calculateSalary() 
		{
			System.out.println("Calculating salary for Temp Employee");		
		}

	}
