package day4.facatorymethodExample;

public abstract class Shape {
	public void acceptDetails()
	{
		System.out.println("Accepting Details");
	}
	public void displayDetails()
	{
		System.out.println("Displaying Details ");
	}
	public abstract void calculateArea();
}
