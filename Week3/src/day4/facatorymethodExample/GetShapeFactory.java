package day4.facatorymethodExample;

public class GetShapeFactory {
	public Shape getShape(String shapetype)
	{
		if(shapetype.equals("Square"))
		{
			return new Square();
		}
		else if(shapetype.equals("Rectangle"))
		{
			return new Rectangle();
		}
		return null;
	}
}
