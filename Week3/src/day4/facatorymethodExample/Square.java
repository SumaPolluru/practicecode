package day4.facatorymethodExample;

public class Square extends Shape {

	@Override
	public void calculateArea() {
		System.out.println("Calculating Area for Square");
	}

}
