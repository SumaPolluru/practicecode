package day4.facatorymethodExample;
import java.util.Scanner;
public class Main {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Shape type Square/Rectangle");
		String name=sc.next();
		GetShapeFactory eshape=new GetShapeFactory();
		Shape eobj=eshape.getShape(name);
		eobj.acceptDetails();
		eobj.displayDetails();
		eobj.calculateArea();
	}
}
