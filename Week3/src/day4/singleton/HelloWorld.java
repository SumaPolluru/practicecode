package day4.singleton;

public class HelloWorld {
	private static HelloWorld hobj;
	public void display()
	{
		System.out.println("Welcome to Hello World Program");
	}
	//Private constructor means no class will be able to access object of class.
	private HelloWorld()
	{
		
	}
	public  static HelloWorld getObj()
	{
		hobj=new HelloWorld();
		return hobj;
	}
}
/*
 * Steps to create singleton class
 * 	1)Create constructor as private
 * 	2) create reference variable of class as static
 *	3)Create one public method which will return this reference
 * *
 */
