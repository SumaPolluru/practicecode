package day4.singletonExample;
public class EmployeeManager {
	private static EmployeeManager hobj;
	public void accept() {
		System.out.println("Welcome to accept employee details");
	}
	public void display()
	{
		System.out.println("Welcome to Singleton Example");
	}
	
	private EmployeeManager()
	{
		
	}
	public  static EmployeeManager getObj()
	{
		hobj=new EmployeeManager();
		return hobj;
	}
}

