package custom.anotationexcercise;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	//annotation object is available at run time also
	public @interface UserDetails{
		public String username();
		public String password();
	}
