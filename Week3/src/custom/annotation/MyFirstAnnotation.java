package custom.annotation;
	import java.lang.annotation.ElementType;
	import java.lang.annotation.Retention;
	import java.lang.annotation.RetentionPolicy;
	import java.lang.annotation.Target;

	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	//annotation object is available at run time also
	public @interface MyFirstAnnotation{
		public int value();//this is property used inside annotation
	}

	/*
	 * The syntax to create annotation is 
	 * public @interface <Annotation_name>
	 */
