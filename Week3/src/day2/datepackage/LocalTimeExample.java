package day2.datepackage;
import java.time.*;
public class LocalTimeExample {
	public static void main(String[] args) {
		LocalTime currenttime=LocalTime.now();
		System.out.println("Current time "+currenttime);
		LocalTime time1=LocalTime.of(10, 45,15);
		System.out.println("Time specified as "+time1.plusHours(1));
		System.out.println("Time was before 20 minutes "+time1.minusMinutes(20));
		
		
		
	}

}
