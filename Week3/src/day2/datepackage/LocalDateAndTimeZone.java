package day2.datepackage;
import java.time.*;
import java.time.temporal.ChronoUnit;
public class LocalDateAndTimeZone {
	public static void main(String[] args) {
		ZoneId zone1=ZoneId.of("Asia/Kolkata");
		ZoneId zone2=ZoneId.of("Asia/Tokyo");
		LocalTime time1=LocalTime.now(zone1);
		System.out.println("Current time in kolkata delhi "+time1);
		LocalTime time2=LocalTime.now(zone2);
		System.out.println("Current time in Tokyo "+time2);
		long hours=ChronoUnit.HOURS.between(time1, time2);
		System.out.println("Hours between time 1 and time 2 is "+hours+" hours");	
	}
}
