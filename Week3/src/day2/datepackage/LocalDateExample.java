package day2.datepackage;
import java.time.*;
public class LocalDateExample {
	public static void main(String[] args) {
		LocalDate date=LocalDate.now();
		System.out.println("Current date is "+date);
		LocalDate yesterday=date.minusDays(1);
		System.out.println("Yesterday date is "+yesterday);
		LocalDate tom=date.plusDays(1);
		System.out.println("Tomorrow date is "+tom);
		System.out.println("Date after 1 week");
		LocalDate weekday=date.plusWeeks(1);
		System.out.println("After one week "+weekday);
		System.out.println("is leap year or not "+date.isLeapYear());
		LocalDate date1=LocalDate.of(2022, 06, 23);
		LocalDate newmonth=date1.plusMonths(1);
		System.out.println("After adding one month to the date1 is "+newmonth);
	}

}
