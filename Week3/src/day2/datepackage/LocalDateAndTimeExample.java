package day2.datepackage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

public class LocalDateAndTimeExample {
	public static void main(String[] args) {
		LocalDateTime now=LocalDateTime.now();
		System.out.println("Before Formatting "+now);
		DateTimeFormatter format=DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		String formattedDateTime=now.format(format);
		System.out.println("Formatted Date and time is "+formattedDateTime);
		System.out.println("current day of week"+now.get(ChronoField.DAY_OF_WEEK));
	}

}
