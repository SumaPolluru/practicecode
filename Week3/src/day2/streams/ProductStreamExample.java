                             package day2.streams;
import java.util.*;
import java.util.stream.*;
public class ProductStreamExample {
	private List<Product> prdlist;
	public ProductStreamExample()
	{
		prdlist=new ArrayList<Product>();
		prdlist.add(new Product(101,"Mouse",1000));
		prdlist.add(new Product(102,"Laptop",70000));
		prdlist.add(new Product(103,"IPAD",55000));
		prdlist.add(new Product(104,"HeadPhone",3500));
		
	}
	public void oldWay()
	{
		for(Product p:prdlist)
		{
			if(p.getPrice()>50000)
			{
				System.out.println("In Old Way: Product name is "+p.getProductname());
			}
		}
	}
	public void newWay()
	{
		List<Product> prdfilteredlist=
prdlist.stream().filter(p->p.getPrice()>50000).collect(Collectors.toList());
		prdfilteredlist.forEach(prd->System.out.println("In New Way: Product name is "+prd.getProductname()));
	/*	forEach(prd->{
			System.out.println("Name is "+prd.getProductname())
			;}
		);*/
	}
	public void streamOperations() {
		double totalPrice=prdlist.stream().filter(p->p.getPrice()>50000)
				.collect(Collectors.summingDouble(p->p.getPrice()));
		System.out.println("Total price is "+totalPrice);
		System.out.println("Finding maximum price");
		Product pmax=prdlist.stream()
	.max((product1,product2)->(product1.getPrice()>product2.getPrice())?1:-1).get();
		System.out.println("Product with maximum price "+pmax.getPrice());
		System.out.println("Finding minimum price");
		Product pmin=prdlist.stream()
	.min((product1,product2)->(product1.getPrice()>product2.getPrice())?1:-1).get();
		System.out.println("Product with minimum price "+pmin.getPrice());
		System.out.println("Displaying the product name using map");
		prdlist.stream().filter(p->p.getPrice()>50000)
		.map(prdlist->prdlist.getProductname())
		.forEach(System.out::println);
		System.out.println("Limiting no of records to display");
		prdlist.stream().map(p->p.getProductname())
		.limit(2).forEach(System.out::println);

	}
	public static void main(String[] args) {
	ProductStreamExample pobj=new ProductStreamExample();
	pobj.oldWay();
	pobj.newWay();
	pobj.streamOperations();
	}
}
