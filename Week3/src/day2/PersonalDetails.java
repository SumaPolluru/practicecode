package day2;

public interface PersonalDetails {
	public void acceptName();

	public void acceptAddress();

	public default void acceptJob() {
		System.out.println("This is default method");
	}

}
