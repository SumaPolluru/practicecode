package userInfo.dao;
public interface TaskDAO {
	public void createTask() throws CustomException;
	public void deleteTask();
	public void updateTask();
	public void retreiveTask();
	public void searchTask();
	public void sortTask();
	public void assignTo();
	public void completionDate();
}
