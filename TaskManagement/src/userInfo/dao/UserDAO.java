package userInfo.dao;
public interface UserDAO {
public void insertDetails();
public void deleteDetails();
public void updateDetails();
public void displayDetails();
public void searchDetails();

}
