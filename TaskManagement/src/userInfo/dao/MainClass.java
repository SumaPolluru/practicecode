package userInfo.dao;
import java.util.*;
public class MainClass {
	static Scanner sc;
	public MainClass()
	{
		sc=new Scanner(System.in);
	}
	public static void main(String[] args) {
		MainClass mainobj=new MainClass();
		System.out.println("Please enter you are belongs to[Client/Visitor]");
		String name1="Client";
		String name2="Visitor";
		String name=sc.next();
		{
			if(name.equalsIgnoreCase(name1))
			{
				System.out.println("Welcome Client");
				System.out.println("Please enter username");
				String username=sc.next();
				System.out.println("Please enter the password[numerics]");
				int password=sc.nextInt();
				System.out.println("Successfully login");
				System.out.println("Please press 1 for Client Operations");
				System.out.println("1.Operations for Client");
				System.out.println("Please press 2 for Task Operations");
				System.out.println("2.Operations of Task");
				System.out.println("please press3 for client operations");
				
			}
			else if(name.equalsIgnoreCase(name2))
			{
				System.out.println("Register for entry");
				System.out.println("Please enter your name");
				String visname=sc.next();
				System.out.println("please enter the current time");
				double time=sc.nextDouble();
		        System.out.println("You are ready now!");
		        System.out.println("Please press 4 for visitor operations");
		        System.out.println("4. Operations for Visitor");
			}
			else {
				System.out.println("Not allowed");
			}
		}

		int choice=sc.nextInt();
		switch(choice)
		{
		case 1:
			   mainobj.usermenu();
		       break;      
		case 2:
			mainobj.taskmenu();
			break;
		case 3:mainobj.clientmenu();
		        break;
				
		case 4: mainobj.visitormenu();
		        break;
		default:
			    System.out.println("Invalid");
		}
		
	}
	public void visitormenu() {
		VisitorDAOImpl vdao=new VisitorDAOImpl();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. See the Task ");
			System.out.println("2. Mark the status of Task");
			System.out.println("3. Completed Tasks");
			System.out.println("4. Incomplete Tasks");
			System.out.println("5. Exit");
			int choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				    vdao.seeTask();
				    break;
			case 2:
					vdao.markTask();
					break;
			case 3:
					vdao.completedTasks();
					break;
			case 4:
					vdao.incompleteTasks();
					break;
			case 5:
				 System.out.println("Successfully logout, Thank You!");
					System.exit(0);
					break;		
			}
		System.out.println("Do u want to continue(y/n)");
		ch=sc.next();
		}
		}
	public void clientmenu() {
		ClientDAOImpl cdao=new ClientDAOImpl();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. Create the task details");
			System.out.println("2. Assign the Task");
			System.out.println("3. Completion Date for the task");
			System.out.println("4. Exit");
			int choice=sc.nextInt();
			switch(choice)
			{
			case 1: try {
				cdao.createTask();
				}
			      catch(CustomException e) {
			    	  System.out.println(e.getMessage());
			      }
			         break;
			case 2: cdao.assignTo();
		           break;
			case 3:
				  cdao.completionDate();
			      break;
			case 4:
				   System.out.println("Successfully logout, Thank You!");
					System.exit(0);
					break;		
		    default:
		    	 System.out.println("Invalid");
			}
		System.out.println("Do u want to continue(y/n)");
		ch=sc.next();
		}
		}
	public void taskmenu()
	{
	TaskDAOImpl pdao=new TaskDAOImpl();
	String ch="y";
	while(ch.equals("y"))
	{
		System.out.println("1. Create Task details");
		System.out.println("2. Delete Task details");
		System.out.println("3. Update Task details");
		System.out.println("4. Retreive Task details");
		System.out.println("5. Search the Task details");
		System.out.println("6. Sort the Task");
		System.out.println("7. Exit");
		int choice=sc.nextInt();
		switch(choice)
		{
		case 1:
			try {
				pdao.createTask();
				}
			      catch(CustomException e) {
			    	  System.out.println(e.getMessage());
			      }			break;
		case 2:
				pdao.deleteTask();
				break;
		case 3:
				pdao.updateTask();
				break;
		case 4:
				pdao.retreiveTask();
				break;
		case 5:
			    pdao.searchTask();
			      break;
		case 6: pdao.sortTask();
		         break;
		case 7:
			 System.out.println("Successfully logout, Thank You!");
				System.exit(0);
				break;		
	    default:
	    	 System.out.println("Invalid");
		}
	System.out.println("Do u want to continue(y/n)");
	ch=sc.next();
	}
	}
		public void usermenu()
		{
		UserDAOImpl pdao=new UserDAOImpl();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. Insert user Details");
			System.out.println("2. Delete user Details");
			System.out.println("3. Update user Details");
			System.out.println("4. Display user Details");
			System.out.println("5. Search user Details");
			int choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				    pdao.insertDetails();
				    break;
			case 2:
					pdao.deleteDetails();
					break;
			case 3:
					pdao.updateDetails();
					break;
			case 4:
					pdao.displayDetails();
					break;
			case 5:
				    pdao.searchDetails();
				    break;
			case 6:
				 System.out.println("Successfully logout, Thank You!");
					System.exit(0);
					break;		
			}
		System.out.println("Do u want to continue(y/n)");
		ch=sc.next();
		}
		}
	}


