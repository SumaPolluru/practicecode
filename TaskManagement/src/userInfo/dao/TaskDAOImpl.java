package userInfo.dao;
import userInfo.pjo.*;
import java.util.Arrays;
import java.util.Scanner;
public class TaskDAOImpl implements TaskDAO {
		private Scanner sc;
		Task taskarr[];
		public TaskDAOImpl()
		{
			sc=new Scanner(System.in);
			taskarr=new Task[2];
			
		}
		public void menu() {
			System.out.println("Enter choice 1 or 0");
			int choice =1;
			while(choice==1)
			{
				System.out.println("1.Add Details");
				System.out.println("2.Display Details");
				System.out.println("3.Update Details");
				System.out.println("4.Delete Details");
				System.out.println("5.Search Details");
				System.out.println("6.Sort the task text");
				int option=sc.nextInt();
				switch(option)
				{
				case 1: System.out.println("Adding details");
						try{
							createTask();
						}
						catch(CustomException e) {
							System.out.println(e.getMessage());
						}
						break;
				case 2: System.out.println("Displaying Details");
						retreiveTask();
						break;
				case 3: System.out.println("updating Details");
				        updateTask();
				        break;
				case 4: System.out.println("Delete Details");
				        deleteTask();
				        break;
				case 5: System.out.println("Search Details");
				        searchTask();
				        break;
				case 6: System.out.println("Sort the task");
				        sortTask();
				        break;
				default :System.out.println("invalid case");
				}
				System.out.println("Do you want to continue");
				choice=sc.nextInt();
			}
		}
		public void createTask() throws CustomException
		{
			for(int index=0;index<taskarr.length;index++)
			{
				taskarr[index]=new Task();
				System.out.println("Enter Task id");
				int id=sc.nextInt();
				if(id<0) {
					throw new CustomException();
				}	
				taskarr[index].setTaskId(sc.nextInt(id));
				System.out.println("Enter Task Name ");
				taskarr[index].setTaskTitle(sc.next());
				System.out.println("Enter Task Description ");
				taskarr[index].setTaskText(sc.next());
				System.out.println("Enter name assigned to");
				taskarr[index].setAssignTo(sc.next());
			}
		}
		public void retreiveTask()
		{
			for(Task obj:taskarr)
			{
				System.out.println("Task id is "+obj.getTaskId());
				System.out.println("Task name is "+obj.getTaskText());
				System.out.println("Task text is "+obj.getTaskTitle());
				System.out.println("Task text is "+obj.getAssignTo());
			}
			
		}
		public void updateTask()
		{
				System.out.println("Enter Task id for which u want to make updation");
				int taskid=sc.nextInt();
				for(Task eobj:taskarr)
				{
					if(eobj.getTaskId()==taskid)
					{
						System.out.println("Enter New Task Title and Task Text ");
						eobj.setTaskTitle(sc.next());
						eobj.setTaskText(sc.next());
						
					}
				}
			
		}
		public void deleteTask()
		{
			System.out.println("Enter task id for which you want to delete ");
			int taskid=sc.nextInt();
			for(Task obj1:taskarr)
			{
				if(obj1.getTaskId()==taskid)
				{
					System.out.println("Deleted Task");
					obj1.setTaskTitle(null);
					obj1.setTaskText(null);
					obj1.setAssignTo(null);
				}
			}
		}
		public void searchTask()
		{
			System.out.println("Enter task title");
			int taskid=sc.nextInt();
			boolean x=false;
			for(Task ob:taskarr)
			{
				if(ob.getTaskId()==taskid)
				{
					x=true;
				}
			}
			if(x)
			{
				System.out.println("Found");
			}
			else
			{
				System.out.println(" Not Found");
			}	
			
		}
		@Override
		public void sortTask() {
			System.out.println("Ascending order");
			for(Task eobj:taskarr)
			{
			String[] arr={eobj.getTaskText()};					
			Arrays.sort(arr);
			System.out.println(Arrays.toString(arr));
			}
			}
			
		@Override
		public void assignTo() {
			System.out.println("Enter Task Id for which u want to assign task");
			int taskId=sc.nextInt();
			for(Task eobj:taskarr)
			{
				if(eobj.getTaskId()==taskId)
				{
					System.out.println("please enter the name, to whom u assign the task");
					String name=sc.next();
					System.out.println("The work is assigned to"+name);
				}
				else
				{
					System.out.println("Entered Task Id is not present");
				}
			}
			}
	            

		@Override
		public void completionDate() {
			System.out.println("Enter Task Id for Completion date");
			int taskId=sc.nextInt();
			for(Task eobj:taskarr)
			{
				if(eobj.getTaskId()==taskId)
				{
				System.out.println("Please enter the completion date for the task");
				String date=sc.nextLine();
				System.out.println("The completion date for this task id is"+date);
				}
			}
			
		}
		public static void main(String[] args) {
			TaskDAOImpl taskmain=new TaskDAOImpl();
			taskmain.menu();
			
		}
}