package userInfo.dao;
import java.util.Scanner;
import userInfo.pjo.Task;
public class ClientDAOImpl implements ClientDAO{
	private Scanner sc;
	Task taskarr[];
	public ClientDAOImpl()
	{
		sc=new Scanner(System.in);
		taskarr=new Task[5];
	}
	@Override
	public void createTask() throws CustomException
	{
		for(int index=0;index<taskarr.length;index++)
		{
			taskarr[index]=new Task();
			System.out.println("Enter Task id");
			int id=sc.nextInt();
			if(id<0) {
				throw new CustomException();
			}
			taskarr[index].setTaskId(sc.nextInt(id));
			System.out.println("Enter Task Name ");
			taskarr[index].setTaskTitle(sc.next());
			System.out.println("Enter Task Description ");
			taskarr[index].setTaskText(sc.next());
			System.out.println("Enter name assigned to");
			taskarr[index].setAssignTo(sc.next());
		}
	}
	@Override
	public void assignTo() {
		System.out.println("Enter Task Id for which u want to assign task");
		int taskId=sc.nextInt();
		for(Task eobj:taskarr)
		{
			if(eobj.getTaskId()==taskId)
			{
				System.out.println("please enter the name, to whom u assign the task");
				String name=sc.next();
				System.out.println("The work is assigned to"+name);
			}
		}
		}
            

	@Override
	public void completionDate() {
		System.out.println("Enter Task Id for Completion date");
		int taskId=sc.nextInt();
		for(Task eobj:taskarr)
		{
			if(eobj.getTaskId()==taskId)
			{
			System.out.println("Please enter the completion date for the task");
			String date=sc.next();
			System.out.println("The completion date for this task id is"+date);
			}
		}
		
	}

}
