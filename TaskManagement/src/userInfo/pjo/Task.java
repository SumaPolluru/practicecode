package userInfo.pjo;

public class Task {
		private int taskId;
		private String taskTitle;
		private String taskText;
		private String assignTo;
		public void setTaskId(int taskId)
		{
			this.taskId=taskId;
			
		}
		public int getTaskId()
		{
			return this.taskId;
		}
		public void setTaskTitle(String taskTitle)
		{
			this.taskTitle=taskTitle;
		
		}
		public String getTaskTitle()
		{
			return this.taskTitle;
		}
		public void setTaskText(String taskText)
		{
			this.taskText=taskText;
		
		}
		public String getTaskText()
		{
			return this.taskText;
		}
		public void setAssignTo(String assignTo)
		{
			this.assignTo=assignTo;
		
		}
		public String getAssignTo()
		{
			return this.assignTo;
		}

	}