package com.gl.mini.service;

import java.util.Scanner;

import com.gl.mini.dao.TaskDAO;
import com.gl.mini.dao.TaskDAOImpl;
import com.gl.mini.pojo.Task;

public class TaskService {
	    private Scanner scobj;
		private TaskDAO taskdao;
		public TaskService()
		{
			scobj=new Scanner(System.in);
			taskdao=new TaskDAOImpl();
		}
		public void insertData()
		{
			System.out.println("Enter no of Task u want to insert");
			int nooftask=scobj.nextInt();
			for(int x=1;x<=nooftask;x++)
			{
				Task tobj=new Task();
				System.out.println("Enter Task id");
				tobj.setTaskId(scobj.nextInt());
				System.out.println("Enter TaskTitle");
				tobj.setTaskTitle(scobj.next());
				System.out.println("Enter Task Text");
				tobj.setTaksText(scobj.next());
				System.out.println("Enter Status ");
				tobj.setStatus(scobj.next());
				System.out.println("Enter Completion time");
				tobj.setCompletionTime(scobj.next());
				System.out.println("Enter Assigned To");
				tobj.setAssignedTo(scobj.next());
				taskdao.insert(tobj);
			}
		}
		public void update() {
			System.out.println("Enter Task id for which u want to make updation");
			int taskid=scobj.nextInt();
			taskdao.update(taskid);
		
		}
		public void deleteData() {
			System.out.println("Enter the task id which u want to delete");
			int taskid=scobj.nextInt();
			taskdao.delete(taskid);
		}
}
