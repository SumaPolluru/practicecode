package com.gl.mini.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.gl.mini.pojo.Task;

public class TaskDAOImpl implements TaskDAO{
		List<Task> tasklist;
		private Scanner sc;
		public TaskDAOImpl()
		{
			tasklist=new ArrayList<Task>();
			sc=new Scanner(System.in);
		}
		@Override
		public void insert(Task t) {

			tasklist.add(t);
		}
		public void update(int taskid) {
			Task t1=null;
			for(Task tupdate:tasklist)
			{
				if(tupdate.getTaskId()==taskid)
				{
					t1=tupdate;
				}
			}
			if(t1!=null)
			{
			System.out.println("Enter new Tasktitle");
			t1.setTaskTitle(sc.next());
			System.out.println("Enter new TaskText");
			t1.setTaksText(sc.next());
			}
			else
			{
				System.out.println("No task with same task id");
			}
		}
		@Override
		public void delete(int taskid) {
			Task t1=null;
			for(Task tdelete:tasklist)
			{
				if(tdelete.getTaskId()==taskid)
				{
					t1=tdelete;
				}
			}
			if(t1!=null)
			{
			System.out.println("Deleted successfully");
			tasklist.remove(taskid);
			}
			else
			{
				System.out.println("No task with same task id");
			}
		}
 	@Override
		public List<Task> retreiveTask() {
			for(Task tdisplay:tasklist)
			{
				System.out.println("Task id is "+tdisplay.getTaskId());
				System.out.println("Task title is "+tdisplay.getTaskTitle());
			}
			return tasklist;
		}
	}
