package com.gl.mini.dao;

import java.util.List;

import com.gl.mini.pojo.Task;

public interface TaskDAO {
	public void insert(Task t);

	public void delete(int taskid);

	public void update(int taskid);

	public List<Task> retreiveTask();

}
