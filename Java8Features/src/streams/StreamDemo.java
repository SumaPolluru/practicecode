package streams;

import java.util.Arrays;
import java.util.List;

public class StreamDemo {
	public static void main(String args[]) {
		List<String> strlist= Arrays.asList("Suma","Demo","Hibernate","Quiz","Suma");
		System.out.println("Counting total Elements");
		long total=strlist.stream().count();
		System.out.println("Count of the elements "+total);
		
		strlist.stream().filter(s->s.startsWith("Su")).forEach(x->System.out.println(x));
		long total1=strlist.stream().filter(x->(x.length()>3)).count();
		System.out.println("Total Count "+total1);
		
	}
}
