package streams.product;

public class Category {
	private String grocery;
	private String stationary;
	private String toilery;
	private String vegetables;

	public String getGrocery() {
		return grocery;
	}

	public void setGrocery(String grocery) {
		this.grocery = grocery;
	}

	public String getStationary() {
		return stationary;
	}

	public void setStationary(String stationary) {
		this.stationary = stationary;
	}

	public String getToilery() {
		return toilery;
	}

	public void setToilery(String toilery) {
		this.toilery = toilery;
	}

	public String getVegetables() {
		return vegetables;
	}

	public void setVegetables(String vegetables) {
		this.vegetables = vegetables;
	}

}