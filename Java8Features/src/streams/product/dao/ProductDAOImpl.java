package streams.product.dao;
import java.util.Scanner;

import streams.product.Product;
public class ProductDAOImpl implements ProductDAO {
	private Scanner sc;
	Product productarr[];
	public ProductDAOImpl()
	{
		sc=new Scanner(System.in);
		productarr=new Product[3];	
	}
	public void menu() 
	{
	ProductDAOImpl emobj=new ProductDAOImpl();
	String ch="y";
	while(ch.equals("y"))
	{
		System.out.println("1.Add the Product details");
		System.out.println("2.Search for Product details");
		System.out.println("3.Display the details of  all Employees");
		System.out.println("4.Exit");
		System.out.println("please enter the operation number,which You want to perform");
		int value=sc.nextInt();
		switch(value) {
		       
		case 1: 
			   emobj.addDetails();
			   break;
		case 2:
			   emobj.searchDetails();
			   break;
		case 3:
			    emobj.displayDetails();
			    break;
		case 4:
			    System.out.println("Successfully logout");
			    System.out.println("Thanks for the using this application");
			    System.exit(0);
			    break;
	   default:
		      System.out.println("Invalid option");
		}
		System.out.println("Do you want to continue[y/n]");
		ch=sc.next();
	}
	}
	@Override
	public void addDetails() {
	System.out.println("--------Welcome to add details!-----------");
	{
	for(int index=0;index<productarr.length;index++) {
			productarr[index]=new Product();
			System.out.println("Enter productId ");
			productarr[index].setProductId(sc.nextInt());
			System.out.println("Enter Product Name ");
			productarr[index].setProductName(sc.next());
			System.out.println("Enter Selling Price  ");
			productarr[index].setSellingPrice(sc.nextFloat());
			System.out.println("Enter Employee Contact no");
			productarr[index].setAvailableQuantity(sc.nextInt());
			
		}

	System.out.println("Successfully added!");
	}
	}
	@Override
	public void searchDetails() {
		System.out.println("Enter product Id");
		int pId=sc.nextInt();
		boolean x=false;
		for(Product obj:productarr)
		{
			if(obj.getProductId()==pId)
			{
				x=true;
			}
		}
		if(x)
		{
			System.out.println("Found in Product data");
		}
		else
		{
			System.out.println("Not found in Product data");
		}	
		
	}

	@Override
	public void displayDetails() {
		for(Product obj:productarr)
		{
			System.out.println("-----Product id is "+obj.getProductId()+"-----");
			System.out.println("Product name is "+obj.getProductName());
			System.out.println("SellingPrice  is "+obj.getSellingPrice());
			System.out.println("availableQuantity is "+obj.getAvailableQuantity());
		}
		
	}
}
