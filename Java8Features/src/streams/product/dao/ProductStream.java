package streams.product.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import streams.product.Product;

public class ProductStream {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		Product p1 = new Product(10, "Apple", 860, 60, "grocery");
		Product p2 = new Product(20, "Banana", 580, 567, "stationary");
		Product p3 = new Product(30, "Powder", 65, 46, "vegetables");
		Product p4 = new Product(40, "Soap", 378, 23, "toilery");
		List<Product> arrlist = new ArrayList<Product>();
		arrlist.add(p1);
		arrlist.add(p2);
		arrlist.add(p3);
		arrlist.add(p4);
		long total = arrlist.stream().count();
		System.out.println("Total Count " + total);

		//Spent Cost
		System.out.println("Enter product Id");
		int pId = sc.nextInt();
		double totalSpentPrice = arrlist.stream().filter(p -> p.getProductId() == pId)
				.collect(Collectors.summingDouble(product -> product.getSellingPrice()));
		System.out.println("Total Sum " + totalSpentPrice);
		
		//Search by Product Name
		System.out.println("Enter Product Name");
		String pname = sc.next();
		arrlist.stream().filter(p -> p.getProductName().equalsIgnoreCase(pname)).forEach(productname -> {
			System.out.println("ProductId "+productname.getProductId());
			System.out.println("Product name " + productname.getProductName());
			System.out.println("Product sellingPrice "+productname.getSellingPrice());
			System.out.println("Product availableQuantity"+productname.getAvailableQuantity());
			System.out.println("Category "+productname.getCategory());
		});

		//Search by Category
		System.out.println("Enter Category");
		String cname = sc.next();
		arrlist.stream().filter(p -> p.getCategory().equalsIgnoreCase(cname)).forEach(categoryname -> {
			
			System.out.println("ProductId "+categoryname.getProductId());
			System.out.println("Product name " + categoryname.getProductName());
			System.out.println("Product sellingPrice "+categoryname.getSellingPrice());
			System.out.println("Product availableQuantity"+categoryname.getAvailableQuantity());
			System.out.println("Category "+categoryname.getCategory());
		});
		
		
		//Retrieve
		arrlist.stream().forEach(product -> {
			System.out.println("ProductId "+product.getProductId());
			System.out.println("Product name " + product.getProductName());
			System.out.println("Product sellingPrice "+product.getSellingPrice());
			System.out.println("Product availableQuantity"+product.getAvailableQuantity());
			System.out.println("Category "+product.getCategory());
		});

	}
}
