package streams.project;
import java.util.*;
import java.util.stream.Collectors;
public class ProjectDemo {
public static void main(String args[]) {
	Project p1=new Project(10,"Streams",8,60000);
	Project p2=new Project(20,"Lambda",5,56778);
	Project p3=new Project(30,"Default Interfaces",6,4678);
	Project p4=new Project(40,"Sorting",3,2300);
	List<Project> arrlist=new ArrayList<Project>();
	arrlist.add(p1);
	arrlist.add(p2);
	arrlist.add(p3);
	arrlist.add(p4);
	long total=arrlist.stream().count();
	System.out.println("Total Count "+total);
	
	arrlist.stream().filter(p->p.getDuration()>5).forEach(System.out::println);
	
	double sum=arrlist.stream().filter(p->p.getProjectid()==10).collect(Collectors.summingDouble(product->product.getCost()));
	System.out.println("Total Sum "+sum);
	
}
}
