package streams.project;

public class Project {
	private int projectid;
	private double cost;

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}

	public String getProjecttitle() {
		return projecttitle;
	}

	public Project(int projectid, String projecttitle, int duration, double cost) {
		super();
		this.projectid = projectid;
		this.projecttitle = projecttitle;
		this.duration = duration;
		this.cost=cost;
	}

	public void setProjecttitle(String projecttitle) {
		this.projecttitle = projecttitle;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	private String projecttitle;
	private int duration;

	public Project() {

	}
}
