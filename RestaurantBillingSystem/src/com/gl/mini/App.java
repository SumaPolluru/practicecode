package com.gl.mini;

import java.io.IOException;

import java.util.*;
import com.gl.mini.dao.AdminDAOImpl;
import com.gl.mini.dao.UserDAOImpl;
import com.gl.mini.exception.InvalidDishEntryException;
import com.gl.mini.exception.InvalidDishIdException;
import com.gl.mini.logger.LogClass;
import com.gl.mini.pojo.Admin;
import com.gl.mini.pojo.User;
import com.gl.mini.service.AdminServiceImpl;
import com.gl.mini.service.UserServiceImpl;

public class App extends Thread 
{
	static Scanner sc;

	public App() //Constructor
	{
		sc = new Scanner(System.in);
	}

	public void run() //Thread
	{
		adminMenu();
		userMenu();
		checkUser();
		checkAdmin();
	}

	public static void main(String[] args) throws SecurityException, IOException {
		LogClass lobj = new LogClass();// Creating object for LogClass
		lobj.logMethod();
		Thread t1 = new Thread();
		t1.start();
		App mainobj = new App(); //Creating Main Object
		UserServiceImpl userobj = new UserServiceImpl();
        System.out.println("******************************************************************");
        System.out.println("******************************************************************");
		System.out.println("...................WELCOME TO SURABHI RESTAURANT....................");
		System.out.println("*******************************************************************");
        System.out.println("*******************************************************************");
		String ch = "null";
		int choice = 0;
		do {
			System.out.println("Please enter you are belongs to");
			System.out.println("1.Admin");
			System.out.println("2.User");
			System.out.println("3.New User(Register)");
			choice = sc.nextInt();
			switch (choice)
			{
				case 1:
						mainobj.checkAdmin();
								break;
				case 2:
						mainobj.checkUser();
								break;
				case 3:
						userobj.registerUser();
						System.out.println("Now,Login with your credentails");
						mainobj.checkUser();
						break;
			default:
						System.out.println("Invalid option");
			}
		} 
		while (ch.equals("y"));
	}

	public void checkUser() // Validating User
	{
		UserDAOImpl daoimpl = new UserDAOImpl(); // Kindly Check the User Credentials in User Table
		System.out.println("Enter User name");
		String userName = sc.next();
		System.out.println("Enter User Password");
		String userPassword = sc.next();
		List<User> userlist = daoimpl.retrieveUser();
		boolean loginStatus = false;
		for (User obj : userlist) 
		{
			if (obj.getUserName().equals(userName) && (obj.getPassword().equals(userPassword))) 
			{
				loginStatus = true;
				break;
			}
		}
		if (loginStatus)
		{
			System.out.println("User is valid: " + userName);
			userMenu();
		} 
		else 
		{
			System.out.println("Login credentials are Invalid!,Please Try again/Register ");
		}
	}

	public void checkAdmin() // Kindly Check the Admin Credentials in Admin Table
	{
		AdminDAOImpl daoimpl = new AdminDAOImpl();
		System.out.println("Enter Admin Name");
		String adminName = sc.next();
		System.out.println("Enter Admin Password");
		String adminPassword = sc.next();
		List<Admin> adminlist = daoimpl.retreiveAdmin();
		boolean loginStatus = false;
		for (Admin obj : adminlist)
		{
			if (obj.getAdminName().equals(adminName) && (obj.getAdminPassword().equals(adminPassword)))
			{
				loginStatus = true;
				break;
			}

		}
		if (loginStatus) 
		{
			System.out.println("Admin is valid " + adminName);
			adminMenu();
		}

		else 
		{
			System.out.println("Login credentials are Invalid");
		}
	}

	public void adminMenu()
	{
		AdminServiceImpl pdao = new AdminServiceImpl();
		String ch = "y";
		while (ch.equals("y"))
		{
			System.out.println("1. Insert the Menu");
			System.out.println("2. Update the Menu");
			System.out.println("3. Delete the Menu");
			System.out.println("4. Display the Menu");
			System.out.println("5. Total Bills Today");
			System.out.println("6. Total Sales in Month");
			System.out.println("7. Logout");
			int choice = sc.nextInt();
			switch (choice) 
			{
				case 1:    	try 
				          {				
							pdao.insertMenu();
				          }
				          catch(InvalidDishIdException e) {
				        	  System.out.println(e.getMessage());
				          }
							break;
				case 2:
						pdao.updateMenu();
							break;
				case 3:
						pdao.deleteMenu();
							break;
				case 4:
						pdao.displayMenu();
							break;
				case 5:
						pdao.displayTodayBills();
							break;
				case 6:
						pdao.displayTotalSaleOfMonth();
							break;
				case 7:
						System.out.println("Successfully logout, Thank You!");
						System.exit(0);
							break;
				default:
						System.out.println("Invalid option");
			}
			System.out.println("Do u want to continue to Admin operations[y/n]");
			ch = sc.next();
		}
	}

	public void userMenu() 
	{
		UserServiceImpl pdao = new UserServiceImpl();
		String ch = "y";
		while (ch.equals("y")) {
			System.out.println("1. Display the Menu");
			System.out.println("2. Order the Item");
			System.out.println("3. Display Total Bill");
			System.out.println("4. Logout");
			int choice = sc.nextInt();
			switch (choice) 
			{
				case 1:
						pdao.displayItems();
							break;
				case 2: 
						try 
						{
							pdao.orderItems();
						} 
						catch (InvalidDishEntryException e)
						{
							System.out.println(e.getMessage());
						}		        
							break;
				case 3:
						pdao.displayTotalBill();
							break;
				case 4:
						System.out.println("Successfully logout, Thank You!");
						System.exit(0);
							break;
			default:
						System.out.println("Invalid option");
			}
			System.out.println("Do u want to continue to user operations[y/n]");
			ch = sc.next();
		}
	}
}
