package com.gl.mini.pojo;

import java.time.LocalDate;

public class Items //Items Class
{
	private LocalDate date; //Local date for getting current date automatically using now method
	private int dishId;
	private String dishName;
	private double pricePerPlate;
	private String quantityInPlates;

	public LocalDate getDate() 
	{
		this.date=LocalDate.now();
		return date;
	}
	public void setDate(LocalDate date)
	{
		this.date=date;
	}
	public int getDishId()
	{
		return dishId;
	}
	public void setDishId(int dishId)
	{
		this.dishId = dishId;
	}

	public String getDishName()
	{
		return dishName;
	}

	public void setDishName(String dishName) 
	{
		this.dishName = dishName;
	}

	public double getPricePerPlate()
	{
		return pricePerPlate;
	}

	public void setPricePerPlate(double pricePerPlate) 
	{
		this.pricePerPlate = pricePerPlate;
	}

	public String getQuantityInPlates() 
	{
		return quantityInPlates;
	}

	public void setQuantityInPlates(String quantityInPlates)
	{
		this.quantityInPlates = quantityInPlates;
	}

}
