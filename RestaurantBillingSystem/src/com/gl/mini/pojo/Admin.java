package com.gl.mini.pojo;

import java.util.Objects;

public class Admin  //Admin Class
{
	private long adminId;
	private String adminName;
	private String adminPassword;

	public long getAdminId()
	{
		return adminId;
	}

	public void setAdminId(long adminId)
	{
		this.adminId = adminId;
	}

	public String getAdminName()
	{
		return adminName;
	}

	public void setAdminName(String adminName) 
	{
		this.adminName = adminName;
	}

	public String getAdminPassword()
	{
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword)
	{
		this.adminPassword = adminPassword;
	}

	@Override
	public int hashCode()  //HashCode method to compare two objects hashcode
	{
		return Objects.hash(adminName, adminPassword);
	}

	@Override
	public boolean equals(Object obj) //Equals method for compare data
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		return Objects.equals(adminName, other.adminName) && 
			   Objects.equals(adminPassword, other.adminPassword);
	}

}
