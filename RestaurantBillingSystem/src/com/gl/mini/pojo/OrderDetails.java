package com.gl.mini.pojo;

import java.time.LocalDate;

public class OrderDetails //OrderDetails Class
{
	private String orderedUserId;
	private int orderedDishId;
	private String orderedDishName;
	private int noOfPlates;
	private LocalDate orderedDate;

	public String getOrderedUserId() 
	{
		return orderedUserId;
	}

	public void setOrderedUserId(String orderedUserId) 
	{
		this.orderedUserId = orderedUserId;
	}
	public int getOrderedDishId()
	{
		return orderedDishId;
	}

	public void setOrderedDishId(int orderedDishId) 
	{
		this.orderedDishId = orderedDishId;
	}

	public String getOrderedDishName()
	{
		return orderedDishName;
	}

	public void setOrderedDishName(String orderedDishName)
	{
		this.orderedDishName = orderedDishName;
	}

	public int getNoOfPlates()
	{
		return noOfPlates;
	}

	public void setNoOfPlates(int noOfPlates)
	{
		this.noOfPlates = noOfPlates;
	}

	public LocalDate getOrderedDate()   //declaring the the date with now method for current date
	{
		this.orderedDate = LocalDate.now();
		return orderedDate;
	}

	public void setOrderedDate(LocalDate orderedDate) 
	{
		this.orderedDate=orderedDate;
		
	}

}