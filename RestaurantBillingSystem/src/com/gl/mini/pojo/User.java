package com.gl.mini.pojo;

import java.util.Objects;

public class User //User Class
{
	private String userId;
	private String userName;
	private String password;
	private String userCompleteAddress;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getUserCompleteAddress()
	{
		return userCompleteAddress;
	}

	public void setUserCompleteAddress(String userCompleteAddress) 
	{
		this.userCompleteAddress = userCompleteAddress;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(password, userName);
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return Objects.equals(password, other.password) && 
			   Objects.equals(userName, other.userName);
	}

}
