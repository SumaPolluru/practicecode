package com.gl.mini.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.gl.mini.connect.DataConnect;
import com.gl.mini.pojo.Admin;
import com.gl.mini.pojo.Items;
import com.gl.mini.pojo.OrderDetails;

public class AdminDAOImpl implements AdminDAO {
	private Connection con1;
	private PreparedStatement stat;

	public AdminDAOImpl() {
		con1 = DataConnect.getConnect();
	}
	@Override
	public List<Admin> retreiveAdmin() //Displaying total Admin details
	{
		List<Admin> adminlist = new ArrayList<Admin>();
		try 
		{
			stat = con1.prepareStatement("select * from Admin");
			ResultSet result = stat.executeQuery();
			while (result.next()) 
			{
				Admin e1 = new Admin();
				e1.setAdminId(result.getLong(1));
				e1.setAdminName(result.getString(2));
				e1.setAdminPassword(result.getString(3));
				adminlist.add(e1);
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}

		return adminlist;
	}
	@Override
	public void insertMenu(Items iobj) //Insert Operation
	{
		try {
			stat = con1.prepareStatement("insert into Items values(?,?,?,?,?)");
			stat.setString(1, String.valueOf(iobj.getDate()));
			stat.setInt(2, iobj.getDishId());
			stat.setString(3, iobj.getDishName());
			stat.setDouble(4, iobj.getPricePerPlate());
			stat.setString(5, iobj.getQuantityInPlates());
			int result = stat.executeUpdate();
			if (result > 0) 
			{
				System.out.println("Item Inserted Successfully");
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

	}

	@Override
	public void updateMenu(Items iobj) //Update operation
	{
		try {
			stat = con1.prepareStatement("update Items set PricePerPlate =? where DishName=?");
			stat.setDouble(1, iobj.getPricePerPlate());
			stat.setString(2, iobj.getDishName());
			int result = stat.executeUpdate();
			if (result > 0) 
			{
				System.out.println("Data updated success");
			}
		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void deleteMenu(Items iobj) //Delete operation
	{
		try {
			stat = con1.prepareStatement("delete from items where DishId=?");
			stat.setInt(1, iobj.getDishId());
			int result = stat.executeUpdate();
			if (result > 0) 
			{
				System.out.println("Data deletion success");
			}
		} 
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}

	}

	@Override
	public void displayMenu(Items odobj) //Display Operation
	{	
		try 
	{
		stat = con1.prepareStatement("select DishId,PricePerPlate,DishName from Items where date=?");
		stat.setString(1, String.valueOf(odobj.getDate()));
		ResultSet result = stat.executeQuery();
		while (result.next())
		{

			System.out.println(result.getInt(1)+"             "+result.getDouble(2)+"               "+result.getString(3));
		}
	} 
	catch (SQLException e)
	{
		e.printStackTrace();
	}
}

	@Override
	public void displayTodayBills(OrderDetails odobj) //Using Joins to get Price information from Items table for order details table to calculate total bill
	{
		try
		{
		stat=con1.prepareStatement("SELECT ORDERDETAILS.ORDERED_USERID,ORDERDETAILS.ORDERED_DISHNAME,ITEMS.PRICEPERPLATE,ORDERDETAILS.NOOFPLATES,ORDERDETAILS.DATE FROM Items INNER JOIN ORDERDETAILS ON OrderDETAILS.ORDERED_DISHNAME=ITEMS.DISHNAME AND ORDERDETAILS.DATE=?");
		stat.setString(1, String.valueOf(odobj.getOrderedDate()));
		ResultSet result=stat.executeQuery();
		double totalBill=0;
		double price=1;
		int noOfPlates=1;
		double finalBill=1;
		boolean status=false;
		while(result.next()) 
		{        
			      System.out.println("....................................");
			      System.out.println("Order User Id: "+result.getString(1));
		          System.out.println("Ordered Dish Name: "+result.getString(2));
				  price=result.getDouble(3);
				  noOfPlates=result.getInt(4); 
				  System.out.println("price of dish: "+price);
				  System.out.println("No Of Plates: "+noOfPlates);
				  System.out.println("Ordered Date: "+result.getString(5));
				  status=true;
				  finalBill=price*noOfPlates;
				  totalBill+=price*noOfPlates;	 //calculating Total bill using joins(More secure in financial matter)
				  System.out.println("Total Bill: "+finalBill);
		} 
		if(status)
		{
			System.out.println(".....................................");
			System.out.println("Sum of all bills: "+totalBill);
		}
	
	}
	catch (SQLException e) 
		{
			e.printStackTrace();
		}
  }
	@Override
	public void displayTotalSaleOfMonth(OrderDetails odobj) //	Adding total no plates are ordered successfully in this month
	{
		try
		{
		stat=con1.prepareStatement("SELECT ORDERDETAILS.ORDERED_USERID,ORDERDETAILS.ORDERED_DISHNAME,ITEMS.PRICEPERPLATE,ORDERDETAILS.NOOFPLATES,ORDERDETAILS.DATE FROM Items INNER JOIN ORDERDETAILS ON OrderDETAILS.ORDERED_DISHNAME=ITEMS.DISHNAME AND (ORDERDETAILS.DATE BETWEEN '2022-09-27' AND ?)");
		stat.setString(1, String.valueOf(odobj.getOrderedDate()));
		ResultSet result=stat.executeQuery();
		double totalSale=0;
		double price=1;
		int noOfPlates=1;
		double finalBill=1;
		boolean status=false;
		while(result.next()) 
		{      
			      System.out.println("....................................");
			      System.out.println("Order User Id: "+result.getString(1));
		          System.out.println("Ordered Dish Name: "+result.getString(2));
				  price=result.getDouble(3);
				  noOfPlates=result.getInt(4); 
				  System.out.println("price of dish: "+price);
				  System.out.println("No Of Plates: "+noOfPlates);
				  System.out.println("Ordered Date :"+result.getString(5));
				  status=true;
				  finalBill=price*noOfPlates;
				  totalSale+=price*noOfPlates;	 //calculating Total bill using joins(More secure in financial matter)
				  System.out.println("Total Bill: "+finalBill);
		}        
		if(status)
		{
			System.out.println(".........................................");
			System.out.println("Total Sale of this month: "+totalSale);
		}
	
	}
	catch (SQLException e) 
		{
			e.printStackTrace();
		}
  }
}
