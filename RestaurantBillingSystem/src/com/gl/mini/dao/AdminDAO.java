package com.gl.mini.dao;

import java.util.List;
import com.gl.mini.pojo.Admin;
import com.gl.mini.pojo.Items;
import com.gl.mini.pojo.OrderDetails;

public interface AdminDAO // Creating abstract methods
{
	public void insertMenu(Items iobj);
	public void updateMenu(Items iobj);
	public void deleteMenu(Items iobj);
	public void displayMenu(Items odobj);
	public List<Admin> retreiveAdmin();
	public void displayTodayBills(OrderDetails odobj);
	public void displayTotalSaleOfMonth(OrderDetails odobj);
}
