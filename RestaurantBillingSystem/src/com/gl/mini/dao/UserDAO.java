package com.gl.mini.dao;

import java.util.List;
import com.gl.mini.pojo.Items;
import com.gl.mini.pojo.OrderDetails;
import com.gl.mini.pojo.User;

public interface UserDAO //Creating Abstract methods
{	 
	public void registerUser(User uobj);
	public void displayItems(Items iobj);
	public void orderItems(OrderDetails iobj);
	public void displayTotalBill(); 
	public List<User> retrieveUser();
}
