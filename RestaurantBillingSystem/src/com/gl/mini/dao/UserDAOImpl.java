package com.gl.mini.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.gl.mini.connect.DataConnect;
import com.gl.mini.pojo.Items;
import com.gl.mini.pojo.OrderDetails;
import com.gl.mini.pojo.User;

public class UserDAOImpl implements UserDAO
{
	private Connection con1;
	private PreparedStatement stat; //Queries execution using JDBC Connectivity
	private Scanner sc;

	public UserDAOImpl()
	{
		con1 = DataConnect.getConnect();
		sc = new Scanner(System.in);
	}

	@Override
	public List<User> retrieveUser() //Retrieving all User details
	{
		List<User> userlist = new ArrayList<User>();
		try 
		{
			stat = con1.prepareStatement("select * from User");
			ResultSet result = stat.executeQuery();
			while (result.next())
			{
				User e1 = new User();
				e1.setUserId(result.getString(1));
				e1.setUserName(result.getString(2));
				e1.setPassword(result.getString(3));
				e1.setUserCompleteAddress(result.getString(4));
				userlist.add(e1);
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return userlist;
	}

	@Override
	public void registerUser(User uobj) //	Register for New Users
	{
		try {
			stat = con1.prepareStatement("insert into User values(?,?,?,?)");
			stat.setString(1, uobj.getUserId());
			stat.setString(2, uobj.getUserName());
			stat.setString(3, uobj.getPassword());
			stat.setString(4, uobj.getUserCompleteAddress());
			int result = stat.executeUpdate();
			if (result > 0) 
			{
				System.out.println("Successfully registered!!");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void displayItems(Items iobj)
	{
		try 
		{
			stat = con1.prepareStatement("select DishId,PricePerPlate,DishName from Items where date=?");
			stat.setString(1, String.valueOf(iobj.getDate()));
			ResultSet result = stat.executeQuery();
			while (result.next())
			{
	
				System.out.println(result.getInt(1)+"             "+result.getDouble(2)+"               "+result.getString(3));
			}
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void orderItems(OrderDetails iobj) 
	{
		try 
		{
			stat = con1.prepareStatement("insert into OrderDetails values (?,?,?,?,?)");
			stat.setString(1, iobj.getOrderedUserId());
			stat.setInt(2, iobj.getOrderedDishId());
			stat.setString(3, iobj.getOrderedDishName());
			stat.setInt(4, iobj.getNoOfPlates());
			stat.setString(5, String.valueOf(iobj.getOrderedDate()));
			int result = stat.executeUpdate();
			if (result > 0) 
			{
				System.out.println("Successfully ordered");
			}
		} 
		catch (SQLException e) 
		{

			e.printStackTrace();
		}

	}

	@Override
	public void displayTotalBill() 
	{
	try
	 {
		stat=con1.prepareStatement("SELECT ORDERDETAILS.ORDERED_USERID,ORDERDETAILS.ORDERED_DISHNAME,ITEMS.PRICEPERPLATE,ORDERDETAILS.NOOFPLATES,ORDERDETAILS.DATE FROM Items INNER JOIN ORDERDETAILS ON OrderDETAILS.ORDERED_DISHNAME=ITEMS.DISHNAME");
		ResultSet result=stat.executeQuery();
		double totalBill=0;
		double price=1;
		int noOfPlates=1;
		boolean status=false;
		System.out.println("Enter User Id[Email]");
		String userId=sc.next();
		while(result.next()) 
		{        
			     String orderedUser=result.getString(1);
			      if(orderedUser.equals(userId))
			      {
		          System.out.println("Ordered Dish Name: "+result.getString(2));
				  price=result.getDouble(3);
				  noOfPlates=result.getInt(4); 
				  System.out.println("Price of dish: "+price);
				  System.out.println("No Of Plates: "+noOfPlates);
				  System.out.println("Ordered Date: "+result.getString(5));
				  status=true;
				  totalBill+=price*noOfPlates;
			      }
			      
		} 
		if(status) {
			System.out.println("Total bill: "+totalBill);
		}
		else {
			System.out.println("Your Details are not available");
		}
		}
	catch (SQLException e) 
			{
				e.printStackTrace();
			}
	}
}
		
