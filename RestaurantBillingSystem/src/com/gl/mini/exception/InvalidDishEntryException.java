package com.gl.mini.exception;

public class InvalidDishEntryException extends Exception{
	@Override
	public String getMessage()
	{
		return "DishId is not present in Today Menu";
	}
}
