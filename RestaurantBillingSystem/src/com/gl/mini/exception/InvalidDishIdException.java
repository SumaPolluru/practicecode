package com.gl.mini.exception;

public class InvalidDishIdException  extends Exception
{
	@Override
	public String getMessage()
	{
		return "Dish Id should not be negative";
	}
}
