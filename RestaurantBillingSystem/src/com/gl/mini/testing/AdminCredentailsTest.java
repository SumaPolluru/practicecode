package com.gl.mini.testing;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.gl.mini.pojo.Admin;
import com.gl.mini.service.AdminServiceImpl;

public class AdminCredentailsTest //JUnit Test Case
{
	AdminServiceImpl sobj;

	@Before
	public void setUp() 
	{
		sobj = new AdminServiceImpl();
	}

	@Test
	public void test() 
	{
		Admin a1 = new Admin(); //Passing values to checkAdmin method
		a1.setAdminName("admin@hcl.com");
		a1.setAdminPassword("@Admin");
		assertTrue(sobj.checkAdmin(a1));

	}

	@After
	public void showMessage() 
	{
		System.out.println("All test done");
	}

}
