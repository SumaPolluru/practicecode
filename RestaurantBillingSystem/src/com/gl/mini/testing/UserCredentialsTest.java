package com.gl.mini.testing;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gl.mini.pojo.User;
import com.gl.mini.service.UserServiceImpl;

public class UserCredentialsTest //JUnit TestCase
{
	UserServiceImpl uobj;

	@Before
	public void setUp()
	{
		uobj = new UserServiceImpl();
	}

	@Test
	public void test()  //passing values to CheckUser method for testing credentials
	{
		User u1 = new User();
		u1.setUserName("Suma");
		u1.setPassword("@Suma12");
		u1.setUserName("Chintu");
		u1.setPassword("@Chintu");
		u1.setUserName("Gayathri");
		u1.setPassword("@Gaye12");
		assertTrue(uobj.checkUser(u1));

	}

	@After
	public void showMessage() 
	{
		System.out.println("All test done");
	}

}
