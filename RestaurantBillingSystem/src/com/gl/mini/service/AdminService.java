package com.gl.mini.service;

import com.gl.mini.exception.InvalidDishIdException;

import com.gl.mini.pojo.Admin;

public interface AdminService //Admin operations Methods
{
	public void insertMenu() throws InvalidDishIdException;
	public void updateMenu();
    public void deleteMenu();
    public void displayMenu();
    public void displayTodayBills();
    public void displayTotalSaleOfMonth();
    public void retrieveAdmin();
    public boolean checkAdmin(Admin a1);
}
