package com.gl.mini.service;

import com.gl.mini.exception.InvalidDishEntryException;
import com.gl.mini.pojo.User;

public interface UserService //Declaring abstract methods
{
	public void registerUser();
	public void retrieveUser();
	public void displayItems();
	public void orderItems() throws  InvalidDishEntryException;
	public void displayTotalBill();
	public boolean checkUser(User u1);

}
