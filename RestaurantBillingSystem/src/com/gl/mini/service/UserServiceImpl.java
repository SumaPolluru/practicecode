package com.gl.mini.service;

import java.time.LocalDate;

import java.util.List;
import java.util.Scanner;
import com.gl.mini.dao.UserDAOImpl;
import com.gl.mini.exception.InvalidDishEntryException;
import com.gl.mini.pojo.Items;
import com.gl.mini.pojo.OrderDetails;
import com.gl.mini.pojo.User;

public class UserServiceImpl implements UserService 
{

	private Scanner sc;
	Items emp;
	UserDAOImpl daoimpl;
	public UserServiceImpl() 
	{
		sc = new Scanner(System.in);
		emp = new Items();
		daoimpl = new UserDAOImpl();
	}
	public void registerUser()  //New User Registration
	{ 
		System.out.println(".............................");
		User user = new User();
		System.out.println("Enter UserId(Email)");
		user.setUserId(sc.next());
		System.out.println("Enter User Name");
		user.setUserName(sc.next());
		System.out.println("Enter Password ");
		user.setPassword(sc.next());
		System.out.println("Enter Address");
		user.setUserCompleteAddress(sc.next());
		daoimpl.registerUser(user);
	}

	@Override
	public void retrieveUser()  
	{
		List<User> userList = daoimpl.retrieveUser();
		for (User uobj : userList) 
		{
			System.out.println("..........................................");
			System.out.println("UserName: " + uobj.getUserName());
			System.out.println("User Password: " + uobj.getPassword());
		}
	}

	@Override
	public boolean checkUser(User u1) //Testing
	{
		boolean result = false;
		List<User> userList = daoimpl.retrieveUser();
		for (User uobj : userList) 
		{
			if (uobj.getUserName().equals(u1.getUserName()) && (uobj.getPassword().equals(u1.getPassword()))) 
			{
				result = true;
				break;
			} 
			else 
			{
				result=false;
			}
		}
		if (result) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	@Override
	public void displayItems() 
	{
		System.out.println("................Today Menu.........................");
		LocalDate date=LocalDate.now();
		System.out.println("Today Date:"+date);
		System.out.println("....................................................");
		Items iobj=new Items();
		iobj.setDate(date);
		System.out.println("DishId     "+"Price Per Plate      "+"DishName");
		System.out.println("....................................................");
		daoimpl.displayItems(iobj);	
	}
	@Override
	public void orderItems() throws InvalidDishEntryException 
	{
		System.out.println(".................Ordering the Items................");
		System.out.println("Enter no of Items u want to order ");
		int noofItems = sc.nextInt();
		for (int x = 1; x <= noofItems; x++) 
		{
			OrderDetails e1 = new OrderDetails();
			System.out.println("Enter Ordered User Id(Email)");
			e1.setOrderedUserId(sc.next());
			System.out.println("Enter DishId for which you want to order");
			int id=sc.nextInt();
			if(id<100 || id>120)
			{
				throw new InvalidDishEntryException();
			}
			e1.setOrderedDishId(id);
			System.out.println("Enter Dish Name in the Menu");
			e1.setOrderedDishName(sc.next());
			System.out.println("Enter No of plates ");
			e1.setNoOfPlates(sc.nextInt());
			daoimpl.orderItems(e1);
		}

		}	
	@Override
	public void displayTotalBill()
	{	
		System.out.println("..............Total Bill...................");
		daoimpl.displayTotalBill();
	}
}
