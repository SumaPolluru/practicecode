package com.gl.mini.service;

import java.time.LocalDate;

import java.util.List;
import java.util.Scanner;
import com.gl.mini.dao.AdminDAOImpl;
import com.gl.mini.exception.InvalidDishIdException;
import com.gl.mini.pojo.Admin;
import com.gl.mini.pojo.Items;
import com.gl.mini.pojo.OrderDetails;


public class AdminServiceImpl implements AdminService
{
	private Scanner sc;
	Items emp;
	AdminDAOImpl daoimpl;

	public AdminServiceImpl() //constructor
	{
		sc = new Scanner(System.in);
		emp = new Items();
		daoimpl = new AdminDAOImpl();
	}
	@Override
	public void displayMenu() //Display Operation
	{
		System.out.println("................Today Menu.................");
		LocalDate date=LocalDate.now();
		System.out.println("Today Date:"+date);
		System.out.println("....................................................");
		Items iobj=new Items();
		iobj.setDate(date);
		System.out.println("DishId     "+"Price Per Plate      "+"DishName");
		System.out.println("....................................................");
		daoimpl.displayMenu(iobj);	
	}

	@Override
    public void retrieveAdmin() //Retrieve Operation
	{
		List<Admin> adminlist = daoimpl.retreiveAdmin();
		for (Admin b1 : adminlist) 
		{
			System.out.println("..........................................");
			System.out.println("AdminName: " + b1.getAdminName());
			System.out.println("Admin Password: " + b1.getAdminPassword());
		}
	}
	@Override
	public boolean checkAdmin(Admin a1) //For Testing
	{
		boolean result=false;
		List<Admin> adminList = daoimpl.retreiveAdmin();//using Admin database data
		for (Admin obj : adminList) {
			if (obj.getAdminName().equals(a1.getAdminName()) && (obj.getAdminPassword().equals(a1.getAdminPassword())))
			{
				result=true;
					break;
			} 
			else 
			{
				return false;
			}
		}
		if(result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	@Override
	public void insertMenu() throws InvalidDishIdException //Menu Insertion Operation 
	{
		System.out.println("Enter no of Items u want to add ");
		int noofItems = sc.nextInt();
		for (int x = 1; x <= noofItems; x++) 
		{
			Items e1 = new Items();
			System.out.println("Enter Dish Id");
            int id=sc.nextInt();
			if(id<0) 
			{
				throw new InvalidDishIdException();
			}
			e1.setDishId(id);
			System.out.println("Enter Dish Name ");
			e1.setDishName(sc.next());
			System.out.println("Enter Price Per Plate ");		
			e1.setPricePerPlate(sc.nextDouble());
			System.out.println("Enter Quantity in plates ");
			e1.setQuantityInPlates(sc.next());
			daoimpl.insertMenu(e1);
		}
	}
	@Override
	public void deleteMenu()  //Delete Operation for Menu
	{
		System.out.println("Enter Dish Id which u want to delete");
		int itemId = sc.nextInt();
		Items deleteItems = new Items();
		deleteItems.setDishId(itemId);
		daoimpl.deleteMenu(deleteItems);
	}
	@Override
	public void updateMenu()   //Update Operation for Menu
	{
		System.out.println("Enter Dish Name which u want to update");
		String dishName = sc.next();
		System.out.println("Enter new Price ");
		double newPrice = sc.nextDouble();
		Items updateList = new Items();
		updateList.setDishName(dishName);
		updateList.setPricePerPlate(newPrice);
		daoimpl.updateMenu(updateList);
	}

	@Override
	public void displayTodayBills() 
	{
		System.out.println("Today Bills");
		LocalDate date=LocalDate.now(); //Setting current date(To filter today bills)
		OrderDetails odobj= new OrderDetails(); 
		odobj.setOrderedDate(date);
		daoimpl.displayTodayBills(odobj);
	}

	@Override
	public void displayTotalSaleOfMonth() 
	{
		System.out.println("Total Sale Of Month in term of plates");
		LocalDate date=LocalDate.now();
		OrderDetails odobj=new OrderDetails(); //Setting two dates for getting Total Sale for month using Between operator
		odobj.setOrderedDate(date);
		LocalDate date1=LocalDate.now();
		date1=date1.plusMonths(1); //adding one month to current date
		odobj.setOrderedDate(date1);
		daoimpl.displayTotalSaleOfMonth(odobj);
	}
}
