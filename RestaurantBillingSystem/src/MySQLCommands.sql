Enter password: ************
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 8.0.30 MySQL Community Server - GPL

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database BillingSystem;
Query OK, 1 row affected (0.01 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| billingsystem      |
| hcl_training       |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| todomanager        |
+--------------------+
7 rows in set (0.02 sec)

mysql> Use BillingSystem;
Database changed
mysql> show tables;
Empty set (0.01 sec)

mysql> create table User(UserId varchar(20) primary key,UserName varchar(20) not null,Password varchar(30) not null,CompleteAddress varchar(80) not null);
Query OK, 0 rows affected (0.03 sec)

mysql> desc user;
+-----------------+-------------+------+-----+---------+-------+
| Field           | Type        | Null | Key | Default | Extra |
+-----------------+-------------+------+-----+---------+-------+
| UserId          | varchar(20) | NO   | PRI | NULL    |       |
| UserName        | varchar(20) | NO   |     | NULL    |       |
| Password        | varchar(30) | NO   |     | NULL    |       |
| CompleteAddress | varchar(80) | NO   |     | NULL    |       |
+-----------------+-------------+------+-----+---------+-------+
4 rows in set (0.00 sec)

mysql> Create table Admin(AdminId int primary key,AdminName varchar(20),AdminPassword varchar(30));
Query OK, 0 rows affected (0.06 sec)

mysql> desc Admin;
+---------------+-------------+------+-----+---------+-------+
| Field         | Type        | Null | Key | Default | Extra |
+---------------+-------------+------+-----+---------+-------+
| AdminId       | int         | NO   | PRI | NULL    |       |
| AdminName     | varchar(20) | YES  |     | NULL    |       |
| AdminPassword | varchar(30) | YES  |     | NULL    |       |
+---------------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql>Create table Items(Date varchar(20) not null,DishID int primary key,DishName varchar(30),PricePerPlate int not null,QuantityInPlates varchar(30) not null);
Query OK, 0 rows affected (0.41 sec)
mysql> show tables;
+-------------------------+
| Tables_in_billingsystem |
+-------------------------+
| admin                   |
| items                   |
| user                    |
+-------------------------+
3 rows in set (0.00 sec)

mysql> desc items;
+------------------+-------------+------+-----+---------+-------+
| Field            | Type        | Null | Key | Default | Extra |
+------------------+-------------+------+-----+---------+-------+
| Date             | varchar(20) | NO   |     | NULL    |       |
| DishID           | int         | NO   | PRI | NULL    |       |
| DishName         | varchar(30) | YES  |     | NULL    |       |
| PricePerPlate    | int         | NO   |     | NULL    |       |
| QuantityInPlates | varchar(30) | NO   |     | NULL    |       |
+------------------+-------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

4 rows in set (0.00 sec)
create table OrderDetails(Ordered_UserId varchar(20) not null,Ordered_DishId int,Ordered_DishName varchar(30) not null,NoofPlates int not null,date varchar(20) not null);
Query OK, 0 rows affected (0.04 sec)

mysql> desc orderdetails;
+------------------+-------------+------+-----+---------+-------+
| Field            | Type        | Null | Key | Default | Extra |
+------------------+-------------+------+-----+---------+-------+
| Ordered_UserId   | varchar(20) | NO   |     | NULL    |       |
| Ordered_DishId   | int         | YES  |     | NULL    |       |
| Ordered_DishName | varchar(30) | NO   |     | NULL    |       |
| NoofPlates       | int         | NO   |     | NULL    |       |
| date             | varchar(20) | NO   |     | NULL    |       |
+------------------+-------------+------+-----+---------+-------+
5 rows in set (0.00 sec)
5 rows in set (0.00 sec)
mysql> alter table orderdetails add constraint fkuserid foreign key(Ordered_UserId) references User(UserId);
Query OK, 0 rows affected (0.06 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> alter table orderdetails add constraint fkdishname foreign key(Ordered_DishId) references items(DishID);
Query OK, 0 rows affected (0.07 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> desc orderdetails;
+------------------+-------------+------+-----+---------+-------+
| Field            | Type        | Null | Key | Default | Extra |
+------------------+-------------+------+-----+---------+-------+
| Ordered_UserId   | varchar(20) | NO   | MUL | NULL    |       |
| Ordered_DishId   | int         | YES  | MUL | NULL    |       |
| Ordered_DishName | varchar(30) | NO   |     | NULL    |       |
| NoofPlates       | int         | NO   |     | NULL    |       |
| date             | varchar(20) | NO   |     | NULL    |       |
+------------------+-------------+------+-----+---------+-------+
5 rows in set (0.00 sec)

mysql> insert into admin values(901080,"admin@hcl.com","@Admin");
Query OK, 1 row affected (0.01 sec)

mysql> select * from admin;
+---------+---------------+---------------+
| AdminId | AdminName     | AdminPassword |
+---------+---------------+---------------+
|  901080 | admin@hcl.com | @Admin        |
+---------+---------------+---------------+
1 row in set (0.00 sec)
mysql> desc user;
+-----------------+-------------+------+-----+---------+-------+
| Field           | Type        | Null | Key | Default | Extra |
+-----------------+-------------+------+-----+---------+-------+
| UserId          | varchar(20) | NO   | PRI | NULL    |       |
| UserName        | varchar(20) | NO   |     | NULL    |       |
| Password        | varchar(30) | NO   |     | NULL    |       |
| CompleteAddress | varchar(80) | NO   |     | NULL    |       |
+-----------------+-------------+------+-----+---------+-------+
4 rows in set (0.00 sec)

mysql> insert into user values("sumagaye@gamil.com","Suma","@Suma12","13/132 SRI RAM NAGAR,PUTTUR(A.P)");
Query OK, 1 row affected (0.00 sec)

mysql> insert into user values("chintu@gmail.com","Chintu","@Chintu","14/173 KB ROAD,TIRUPATI(A.P)");
Query OK, 1 row affected (0.00 sec)

mysql> insert into user values("Gaye2gmail.com","Gayathri","@Gaye12","67/901 KLM CIRCLE HYDERABAD");
Query OK, 1 row affected (0.00 sec)

mysql> SELECT * FROM USER;
+--------------------+----------+----------+----------------------------------+
| UserId             | UserName | Password | CompleteAddress                  |
+--------------------+----------+----------+----------------------------------+
| chintu@gmail.com   | Chintu   | @Chintu  | 14/173 KB ROAD,TIRUPATI(A.P)     |
| Gaye2gmail.com     | Gayathri | @Gaye12  | 67/901 KLM CIRCLE HYDERABAD      |
| sumagaye@gamil.com | Suma     | @Suma12  | 13/132 SRI RAM NAGAR,PUTTUR(A.P) |
+--------------------+----------+----------+----------------------------------+
3 rows in set (0.00 sec)
mysql> insert into items values("2022-09-27",102,"GobiRice",180,50);
Query OK, 1 row affected (0.01 sec)

mysql> insert into items values("2022-09-27",103,"MuttonBiriyani",280,80);
Query OK, 1 row affected (0.00 sec)

mysql> insert into items values("2022-09-27",104,"VegetableRice",170,50)
    -> ;
Query OK, 1 row affected (0.00 sec)

mysql> insert into items values("2022-09-28",105,"Idly",50,90);
Query OK, 1 row affected (0.00 sec)

mysql> insert into items values("2022-09-28",106,"Poori",80,50);
Query OK, 1 row affected (0.00 sec)

mysql> insert into items values("2022-09-28",107,"Pongal",60,30);
Query OK, 1 row affected (0.00 sec)

mysql> insert into items values("2022-09-28",108,"Upma",80,20);
Query OK, 1 row affected (0.00 sec)

mysql> select * from items;
+------------+--------+----------------+---------------+------------------+
| Date       | DishID | DishName       | PricePerPlate | QuantityInPlates |
+------------+--------+----------------+---------------+------------------+
| 2022-09-27 |    101 | Biriyani       |           250 | 100              |
| 2022-09-27 |    102 | GobiRice       |           180 | 50               |
| 2022-09-27 |    103 | MuttonBiriyani |           280 | 80               |
| 2022-09-27 |    104 | VegetableRice  |           170 | 50               |
| 2022-09-28 |    105 | Idly           |            50 | 90               |
| 2022-09-28 |    106 | Poori          |            80 | 50               |
| 2022-09-28 |    107 | Pongal         |            60 | 30               |
| 2022-09-28 |    108 | Upma           |            80 | 20               |
+------------+--------+----------------+---------------+------------------+
8 rows in set (0.00 sec)

mysql> desc orderdetails;
+------------------+-------------+------+-----+---------+-------+
| Field            | Type        | Null | Key | Default | Extra |
+------------------+-------------+------+-----+---------+-------+
| Ordered_UserId   | varchar(20) | NO   | MUL | NULL    |       |
| Ordered_DishId   | int         | YES  | MUL | NULL    |       |
| Ordered_DishName | varchar(30) | NO   |     | NULL    |       |
| NoofPlates       | int         | NO   |     | NULL    |       |
| date             | varchar(20) | NO   |     | NULL    |       |
+------------------+-------------+------+-----+---------+-------+
5 rows in set (0.01 sec)

mysql> insert into orderdetails values("suma@gmail.com",101,"biriyani",3,"2022-09-27");
Query OK, 1 row affected (0.00 sec)

mysql> insert into orderdetails values("suma@gmail.com",102,"GobiRice",2,"2022-09-27");
Query OK, 1 row affected (0.00 sec)

mysql> insert into orderdetails values("chintu@gmail.com",102,"GobiRice",2,"2022-09-27");
Query OK, 1 row affected (0.01 sec)

mysql> select * from orderdetails;
+------------------+----------------+------------------+------------+------------+
| Ordered_UserId   | Ordered_DishId | Ordered_DishName | NoofPlates | date       |
+------------------+----------------+------------------+------------+------------+
| suma@gmail.com   |            101 | biriyani         |          3 | 2022-09-27 |
| suma@gmail.com   |            102 | GobiRice         |          2 | 2022-09-27 |
| chintu@gmail.com |            102 | GobiRice         |          2 | 2022-09-27 |
+------------------+----------------+------------------+------------+------------+
3 rows in set (0.00 sec)
mysql> select * from admin;
+---------+---------------+---------------+
| AdminId | AdminName     | AdminPassword |
+---------+---------------+---------------+
|  901080 | admin@hcl.com | @Admin        |
+---------+---------------+---------------+
1 row in set (0.00 sec)

mysql> select * from user;
+--------------------+----------+----------+----------------------------------+
| UserId             | UserName | Password | CompleteAddress                  |
+--------------------+----------+----------+----------------------------------+
| chintu@gmail.com   | Chintu   | @Chintu  | 14/173 KB ROAD,TIRUPATI(A.P)     |
| Gaye2gmail.com     | Gayathri | @Gaye12  | 67/901 KLM CIRCLE HYDERABAD      |
| sumagaye@gamil.com | Suma     | @Suma12  | 13/132 SRI RAM NAGAR,PUTTUR(A.P) |
+--------------------+----------+----------+----------------------------------+
3 rows in set (0.00 sec)