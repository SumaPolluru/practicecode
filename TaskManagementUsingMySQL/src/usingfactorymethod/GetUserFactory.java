package usingfactorymethod;

public class GetUserFactory {
		public User getUser(String emptype)
		{
			if(emptype.equals("Client"))
			{
				return new Client();
			}
			else if(emptype.equals("Visitor"))
			{
				return new Visitor();
			}
			return null;

		}
	}

