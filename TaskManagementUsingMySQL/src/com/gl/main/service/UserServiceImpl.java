package com.gl.main.service;

import com.gl.main.dao.*;
import com.gl.main.pojo.Client;
import com.gl.main.pojo.Task;
import com.gl.main.pojo.User;

import java.util.*;

public class UserServiceImpl implements UserService {

	private Scanner sc;
	User tobj;
	UserDAOImpl daoimpl;

	public UserServiceImpl() {
		sc = new Scanner(System.in);
		tobj = new User();
		daoimpl = new UserDAOImpl();
	}

	@Override
	public void insertUser() {
		System.out.println("Enter no of User u want to add ");
		int noofuser = sc.nextInt();
		for (int x = 1; x <= noofuser; x++) {
			User e1 = new User();
			System.out.println("Enter User ID");
			e1.setUserId(sc.nextInt());
			System.out.println("Enter Username ");
			e1.setUserName(sc.next());
			System.out.println("Enter password ");
			e1.setPassword(sc.next());
			daoimpl.insertUser(e1);
		}

	}

	@Override
	public void deleteUser() {
		System.out.println("Enter User ID which u want to delete");
		int userId = sc.nextInt();
		User deleteuser = new User();
		deleteuser.setUserId(userId);
		// updateemployee.setEmployeeid();
		daoimpl.deleteUser(deleteuser);

	}

	@Override
	public void updateUser() {
		System.out.println("Enter Task ID which u want to update");
		int userId = sc.nextInt();
		System.out.println("Enter new Salary ");
		String newPassword = sc.next();
		User updateuser = new User();
		updateuser.setUserId(userId);
		updateuser.setPassword(newPassword);
		daoimpl.updateUser(updateuser);
	}

	@Override
	public void displayUser() {
		List<User> tlist = daoimpl.displayUser();
		for (User t1 : tlist) {
			System.out.println("User ID: " + t1.getUserId());
			System.out.println("UserName: " + t1.getUserName());
			System.out.println("User password: " + t1.getPassword());
		}

	}

}
