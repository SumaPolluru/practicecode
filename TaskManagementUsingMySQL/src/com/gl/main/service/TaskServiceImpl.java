package com.gl.main.service;

import com.gl.main.dao.*;
import com.gl.main.exception.CustomException;
import com.gl.main.pojo.Task;
import java.time.LocalDate;
import java.util.*;

public class TaskServiceImpl implements TaskService {

	private Scanner sc;
	Task tobj;
	TaskDAOImpl daoimpl;

	public TaskServiceImpl() {
		sc = new Scanner(System.in);
		tobj = new Task();
		daoimpl = new TaskDAOImpl();
	}

	@Override
	public void insertTask() throws CustomException {
		System.out.println("Enter no of Tasks u want to add ");
		int nooftasks = sc.nextInt();
		for (int x = 1; x <= nooftasks; x++) {
			Task e1 = new Task();
			System.out.println("Enter Task ID");
			int id=sc.nextInt();
			if(id<0) {
				throw new CustomException();
			}
			e1.setTaskId(id);
			System.out.println("Enter Task Title ");
			e1.setTaskTitle(sc.next());
			System.out.println("Enter Task Text ");
			e1.setTaskText(sc.next());
			System.out.println("Whom to assign the task! ");
			e1.setTaskassignedTo(sc.next());
            System.out.println("Enter Task Completion Date[DD/MM/YYYY]");
            e1.setCompletionDate(sc.next());
            System.out.println("Enter Task Completion Time[HH:MM:SS]");
            e1.setCompletionTime(sc.next());
			daoimpl.insertTask(e1);
            
		}

	}

	@Override
	public void deleteTask() {
		System.out.println("Enter Task ID which u want to delete");
		int taskId = sc.nextInt();
		Task deletetask = new Task();
		deletetask.setTaskId(taskId);
		// updateemployee.setEmployeeid();
		daoimpl.deleteTask(deletetask);

	}

	@Override
	public void updateTask() {
		System.out.println("Enter Task ID which u want to update");
		int taskId = sc.nextInt();
		System.out.println("Enter new TaskTitle ");
		String newTaskTitle = sc.next();
		Task updatetask = new Task();
		updatetask.setTaskId(taskId);
		updatetask.setTaskTitle(newTaskTitle);
		daoimpl.updateTask(updatetask);
	}

	@Override
	public void displayTask() {
		List<Task> tlist = daoimpl.retreiveData();
		for (Task t1 : tlist) {
			System.out.println("Task ID: " + t1.getTaskId());
			System.out.println("Task Title: " + t1.getTaskTitle());
			System.out.println("Task Text: " + t1.getTaskText());
			System.out.println("Task assigned to: " + t1.getTaskassignedTo());
			System.out.println("Task Completion Date: "+t1.getCompletionDate());
			System.out.println("Task Completion Time: "+t1.getCompletionTime());
		}

	}

	@Override
	public void searchTask() {
		System.out.println("Enter Task ID which u want to search");
		int taskId = sc.nextInt();
		Task searchtask = new Task();
		searchtask.setTaskId(taskId);
		daoimpl.searchTask(searchtask);

	}

	@Override
	public void completionDate() {
		System.out.println("Enter Task ID which u want to add Completion Date");
		int taskId = sc.nextInt();
		System.out.println("Enter Completion Date[DD/MM/YYYY] ");
		String newcmDate = sc.next();
		Task updatetask = new Task();
		updatetask.setTaskId(taskId);
		updatetask.setTaskTitle(newcmDate);
		daoimpl.completionDate(updatetask);
	}

	@Override
	public void assignTo() {
	System.out.println("Enter Task Id For Task Assignation");
	int taskId = sc.nextInt();
	System.out.println("Whom u want to assign work(name)");
	String name=sc.next();
	Task tassign=new Task();
	tassign.setTaskId(taskId);
	tassign.setTaskassignedTo(name);
	daoimpl.assignTo(tassign);
	}

	@Override
	public void sortData() {
		System.out.println("Sorting the Task");
		List<Task> tlist = daoimpl.sortData();
		for (Task t1 : tlist) {
			System.out.println("Task ID: " + t1.getTaskId());
			System.out.println("Task Title: " + t1.getTaskTitle());
			System.out.println("Task Text: " + t1.getTaskText());
			System.out.println("Task assigned to: " + t1.getTaskassignedTo());
			System.out.println("Task Completion Date: "+t1.getCompletionDate());
			System.out.println("Task Completion Time: "+t1.getCompletionTime());
		}	
	}
}
