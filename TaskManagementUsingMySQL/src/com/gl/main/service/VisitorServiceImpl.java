package com.gl.main.service;

import java.util.Scanner;

import com.gl.main.dao.TaskDAOImpl;
import com.gl.main.dao.VisitorDAOImpl;
import com.gl.main.pojo.Task;
import com.gl.main.pojo.Visitor;

public class VisitorServiceImpl implements VisitorService {
	private Scanner sc;
	Task tobj;
	VisitorDAOImpl daoimpl;

	public VisitorServiceImpl() {
		sc = new Scanner(System.in);
		tobj = new Task();
		daoimpl = new VisitorDAOImpl();
	}

	@Override
	public void seeTask() {
		System.out.println("Enter Task ID which u want to see the task");
		int taskId = sc.nextInt();
		Visitor seetask = new Visitor();
		seetask.setTaskId(taskId);
		daoimpl.seeTask(seetask);
		
	}

	@Override
	public void markTask() {
		System.out.println("Enter Task ID which u want to update");
		int taskId = sc.nextInt();
		System.out.println("Enter Visitor Name");
		String visname = sc.next();
		System.out.println("Enter Mark Status");
		String status=sc.next();
		Visitor updatetask = new Visitor();
		updatetask.setTaskId(taskId);
		updatetask.setVisitorName(visname);
		updatetask.setMarkstatus(status);
		daoimpl.markTask(updatetask);
		
	}

}
