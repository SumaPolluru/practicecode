package com.gl.main.service;

public interface UserService {
	public void insertUser();
	public void deleteUser();
	public void updateUser();
	public void displayUser();
}
