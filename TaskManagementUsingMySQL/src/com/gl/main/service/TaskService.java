package com.gl.main.service;

import com.gl.main.exception.CustomException;

public interface TaskService {
	public void insertTask() throws CustomException;
	public void deleteTask();
	public void updateTask();
	public void displayTask();
	public void searchTask();
	public void completionDate();
	public void assignTo();
	public void sortData();
}
