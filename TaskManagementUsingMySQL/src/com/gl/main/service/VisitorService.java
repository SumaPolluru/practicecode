package com.gl.main.service;

public interface VisitorService {
	public void seeTask();
	public void markTask();
}
