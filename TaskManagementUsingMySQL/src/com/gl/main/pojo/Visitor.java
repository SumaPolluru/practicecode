package com.gl.main.pojo;

public class Visitor extends User{
	private int visitorId;
	private String visitorName;
	private String mobileno;
	private String visitinghours;
	private int user_ID;
	private String markstatus;
	private int taskId;

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getMarkstatus() {
		return markstatus;
	}

	public void setMarkstatus(String markstatus) {
		this.markstatus = markstatus;
	}

	public int getVisitorId() {
		return visitorId;
	}

	public void setVisitorId(int visitorId) {
		this.visitorId = visitorId;
	}

	public String getVisitorName() {
		return visitorName;
	}

	public void setVisitorName(String visitorName) {
		this.visitorName = visitorName;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getVisitinghours() {
		return visitinghours;
	}

	public void setVisitinghours(String visitinghours) {
		this.visitinghours = visitinghours;
	}

	public int getUser_ID() {
		return user_ID;
	}

	public void setUser_ID(int user_ID) {
		this.user_ID = user_ID;
	}

}
