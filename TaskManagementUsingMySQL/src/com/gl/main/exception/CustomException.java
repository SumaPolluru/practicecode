package com.gl.main.exception;

public class CustomException extends Exception
{
	@Override
	public String getMessage()
	{
		return "TaskId should not be negative";
	}
}
