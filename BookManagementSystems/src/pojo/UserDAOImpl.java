package pojo;

import java.util.Scanner;

//Here,Class UserDAOImpl is implements from interface UserDAO
public class UserDAOImpl implements UserDAO {
	private Scanner sc;
	Book newBooks[];
	Book favouriteBooks[];// Here,Object of array is declaring to store book objects
	Book completedBooks[];

	public UserDAOImpl() {
		sc = new Scanner(System.in);
		newBooks = new Book[5];
		favouriteBooks = new Book[5]; // Here,Initializing the object of array to store book objects
		completedBooks = new Book[5];
		newBooks[0] = new Book();
		newBooks[1] = new Book();
		newBooks[2] = new Book();
		newBooks[3] = new Book();
		newBooks[4] = new Book();
		newBooks[0].setBookId(10);// Initializing the book objects
		newBooks[0].setBookName("Wings of fire");
		newBooks[0].setAuthorName("Dr. Abdul Kalam");
		newBooks[0].setDescription("This book explore important issues of family,friendship with strong themes");
		newBooks[1].setBookId(20);
		newBooks[1].setBookName("The Key to Success");
		newBooks[1].setAuthorName("Jim Rohn");
		newBooks[1].setDescription("This book explores,focus with a positive attitude and Believe in yourself ");
		newBooks[2].setBookId(30);
		newBooks[2].setBookName("You Can");
		newBooks[2].setAuthorName("George Adams");
		newBooks[2].setDescription("This book promote personal growth and well-being");
		newBooks[3].setBookId(40);
		newBooks[3].setBookName("A Million Thoughts");
		newBooks[3].setAuthorName("Om Swami");
		newBooks[3].setDescription("This book shows how to meditate correctly");
		newBooks[4].setBookId(50);
		newBooks[4].setBookName("Believe in Yourself");
		newBooks[4].setAuthorName("Joseph Murphy");
		newBooks[4].setDescription("This book is a self-help book about self-esteem and confidence");

		favouriteBooks[0] = new Book();
		favouriteBooks[1] = new Book();
		favouriteBooks[2] = new Book();
		favouriteBooks[3] = new Book();
		favouriteBooks[4] = new Book();

		favouriteBooks[0].setBookId(60);
		favouriteBooks[0].setBookName("The Mind and its Control");
		favouriteBooks[0].setAuthorName("Swami Budhananda");
		favouriteBooks[0].setDescription("This book is an excellent compendium of teachings from Yoga-sutras.");
		favouriteBooks[1].setBookId(70);
		favouriteBooks[1].setBookName("Master Your Emotions");
		favouriteBooks[1].setAuthorName("Leonard");
		favouriteBooks[1].setDescription("This book is practical guide to overcome negativity ");
		favouriteBooks[2].setBookId(80);
		favouriteBooks[2].setBookName("Think and Grow Rich");
		favouriteBooks[2].setAuthorName("Napoleon Hill");
		favouriteBooks[2].setDescription("This book promote wealthy and successful people common habits");
		favouriteBooks[3].setBookId(90);
		favouriteBooks[3].setBookName("Atomic Habits");
		favouriteBooks[3].setAuthorName("James Clear");
		favouriteBooks[3].setDescription("This book definitive guide to breaking bad behaviors");
		favouriteBooks[4].setBookId(100);
		favouriteBooks[4].setBookName("One Day life will Change");
		favouriteBooks[4].setAuthorName("Saranya Umakanthan");
		favouriteBooks[4].setDescription("This book about a story of love and inspiration to win life");

		completedBooks[0] = new Book();
		completedBooks[1] = new Book();
		completedBooks[2] = new Book();
		completedBooks[3] = new Book();
		completedBooks[4] = new Book();

		completedBooks[0].setBookId(110);
		completedBooks[0].setBookName("Do It Today");
		completedBooks[0].setAuthorName("Darius Foroux");
		completedBooks[0].setDescription("This book explore the goals is of big help ");
		completedBooks[1].setBookId(120);
		completedBooks[1].setBookName("Mindest");
		completedBooks[1].setAuthorName("Carol Dweck");
		completedBooks[1].setDescription(
				"This book explores,success comes from having the right mindset rather than intelligence");
		completedBooks[2].setBookId(130);
		completedBooks[2].setBookName("The Secret");
		completedBooks[2].setAuthorName("George Adams");
		completedBooks[2].setDescription("This book promote personal growth and well-being");
		completedBooks[3].setBookId(140);
		completedBooks[3].setBookName("A Girl that had to be Strong");
		completedBooks[3].setAuthorName("Om Swami");
		completedBooks[3].setDescription("This book shows how to meditate correctly");
		completedBooks[4].setBookId(150);
		completedBooks[4].setBookName("The Practicing Mind");
		completedBooks[4].setAuthorName("Joseph Murphy");
		completedBooks[4].setDescription("This book is a self-help book about self-esteem and confidence");

	}

	@Override
	public void seeNewBooksUser1() // Methods using to declare and get objects
	{
		System.out.println("Username : Suma");
		System.out.println("!--------Your New books are--------!");
		newBooks[0].showBooks();
		newBooks[1].showBooks();
	}

	@Override
	public void seeNewBooksUser2() {
		System.out.println("Username : Polluru");
		System.out.println("!--------Your New books are--------!");
		newBooks[2].showBooks();
		newBooks[3].showBooks();
	}

	@Override
	public void seeNewBooksUser3() {
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your New books are--------!");
		newBooks[4].showBooks();
		newBooks[2].showBooks();
	}

	@Override
	public void seeFavouriteBooksUser1() {
		System.out.println("Username : Suma");
		System.out.println("!--------Your Favourite books are--------!");
		favouriteBooks[0].showBooks();
		favouriteBooks[1].showBooks();
	}

	@Override
	public void seeFavouriteBooksUser2() {
		System.out.println("Username : Polluru");
		System.out.println("!--------Your Favourite books are--------!");
		favouriteBooks[2].showBooks();
		favouriteBooks[3].showBooks();
	}

	@Override
	public void seeFavouriteBooksUser3() {
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your Favourite books are--------!");
		favouriteBooks[4].showBooks();
		favouriteBooks[2].showBooks();
	}

	@Override
	public void seeCompletedBooksUser1() {
		System.out.println("Username : Suma");
		System.out.println("!--------Your Completed books are--------!");
		completedBooks[0].showBooks();
		completedBooks[1].showBooks();
	}

	@Override
	public void seeCompletedBooksUser2() {
		System.out.println("Username : Polluru");
		System.out.println("!--------Your Completed books are--------!");
		completedBooks[2].showBooks();
		completedBooks[3].showBooks();
	}

	@Override
	public void seeCompletedBooksUser3() {
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your Completed books are--------!");
		completedBooks[4].showBooks();
		completedBooks[2].showBooks();
	}

	@Override
	public void getBookDetailsUser1() {
		System.out.println("Username : Suma");
		System.out.println("!--------Your Complete books details are--------!");
		newBooks[0].showDetailsOfBooks();
		newBooks[1].showDetailsOfBooks();
		completedBooks[0].showDetailsOfBooks();
		completedBooks[1].showDetailsOfBooks();
		favouriteBooks[0].showDetailsOfBooks();
		favouriteBooks[1].showDetailsOfBooks();
	}

	@Override
	public void getBookDetailsUser2() {
		System.out.println("Username : Polluru");
		System.out.println("!--------Your Complete books details are--------!");
		newBooks[2].showDetailsOfBooks();
		newBooks[3].showDetailsOfBooks();
		completedBooks[2].showDetailsOfBooks();
		completedBooks[3].showDetailsOfBooks();
		favouriteBooks[2].showDetailsOfBooks();
		favouriteBooks[3].showDetailsOfBooks();
	}

	@Override
	public void getBookDetailsUser3() {
		System.out.println("Username : Gayathri");
		System.out.println("!--------Your Complete books details are--------!");
		newBooks[4].showDetailsOfBooks();
		newBooks[2].showDetailsOfBooks();
		completedBooks[4].showDetailsOfBooks();
		completedBooks[2].showDetailsOfBooks();
		favouriteBooks[4].showDetailsOfBooks();
		favouriteBooks[2].showDetailsOfBooks();
	}

	@Override
	public void selectBookUser1() {
		int arr[] = { 10, 20, 60, 70, 110, 120 };
		System.out.println("Please enter Book ID");
		int bookID = sc.nextInt();
		switch (bookID) {
		case 10:

			newBooks[0].showBooks();
			break;
		case 20:

			newBooks[1].showBooks();
			break;
		case 60:

			favouriteBooks[0].showBooks();
			break;
		case 70:

			favouriteBooks[1].showBooks();
			break;
		case 110:

			completedBooks[0].showBooks();
			break;
		case 120:

			completedBooks[1].showBooks();
			break;
		default:
			System.out.println("Book Id is not present");
		}
	}

	@Override
	public void selectBookUser2() {
		int arr[] = { 30, 40, 80, 90, 130, 140 };
		System.out.println("Please enter Book ID");
		int bookID = sc.nextInt();
		switch (bookID) {
		case 30:
			newBooks[2].showBooks();
			break;
		case 40:
			newBooks[3].showBooks();
			break;
		case 80:
			favouriteBooks[2].showBooks();
			break;
		case 90:
			favouriteBooks[3].showBooks();
			break;
		case 130:
			completedBooks[2].showBooks();
			break;
		case 140:
			favouriteBooks[3].showBooks();
			break;
		default:
			System.out.println("Book Id is not present");

		}
	}

	@Override
	public void selectBookUser3() {
		int arr[] = { 50, 30, 80, 100, 130, 150 };
		System.out.println("Please enter Book ID");
		int bookID = sc.nextInt();
		switch (bookID) {
		case 50:
			newBooks[4].showBooks();
			break;
		case 30:
			newBooks[2].showBooks();
			break;
		case 100:
			favouriteBooks[4].showBooks();
			break;
		case 80:
			favouriteBooks[2].showBooks();
			break;
		case 150:
			completedBooks[4].showBooks();
			break;
		case 130:
			completedBooks[2].showBooks();
			break;
		default:
			System.out.println("Book Id is not present");

		}
	}
}
