package pojo;
//Here,Creating the interface
public interface UserDAO 
{
	public void seeNewBooksUser1(); //These are the methods created for perform operations
	public void seeNewBooksUser2();
	public void seeNewBooksUser3();
	public void seeFavouriteBooksUser1();
	public void seeFavouriteBooksUser2();
	public void seeFavouriteBooksUser3();
	public void seeCompletedBooksUser1();
	public void seeCompletedBooksUser2();
	public void seeCompletedBooksUser3();
	public void selectBookUser1();
	public void selectBookUser2();
	public void selectBookUser3();
	public void getBookDetailsUser1();
	public void getBookDetailsUser2();
	public void getBookDetailsUser3();
}
