
package pojo;
//Here,The Class name is Book
public class Book
{
	private String bookName;
	private String authorName;
    private String description;
    private int bookId;
	public String getBookName()
	{
		return bookName;
	}

	public void setBookName(String bookName) 
	{
		this.bookName = bookName;
	}

	public String getAuthorName() 
	{
		return authorName;
	}

	public void setAuthorName(String authorName) 
	{
		this.authorName = authorName;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public int getBookId() 
	{
		return bookId;
	}

	public void setBookId(int bookId) 
	{
		this.bookId = bookId;
	}
	public void showBooks() //This Method is using for returning the result
	{
		System.out.println("BookId:"+bookId+"      "+"\nBookName:"+bookName);
	}
	public void showDetailsOfBooks() //This Method for complete details of book
	{
		System.out.println("BookId:"+bookId+"    "+"\nBookName:"+bookName+"    "+"\nAuthorName:"+authorName+"     "+"\nDescription:"+description);
	}
 }